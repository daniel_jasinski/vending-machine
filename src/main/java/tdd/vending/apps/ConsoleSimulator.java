package tdd.vending.apps;

import tdd.vending.hardware.*;
import tdd.vending.machine.*;
import tdd.vending.money.*;
import tdd.vending.products.*;
import java.io.*;
import java.util.Optional;
import java.util.function.IntConsumer;
import java.util.regex.*;

import static java.lang.Integer.parseInt;

public class ConsoleSimulator
{
    private final static int NUMBER_OF_SHELVES = 10;
    private final static int SHELF_SIZE = 10;

    private final static String CANCEL = "C";
    private final static String INSERT = "I";
    private final static String SHELF = "S";
    private final static String BALANCE = "B";
    private final static String PRODUCTS = "P";
    private final static String QUIT = "Q";

    private final static Pattern COIN_PATTERN =
        Pattern.compile(INSERT+"\\s+(?:(\\d+)[.,])?(\\d+)", Pattern.CASE_INSENSITIVE);
    private final static Pattern SHELF_PATTERN =
        Pattern.compile(SHELF+"\\s+(\\d+)", Pattern.CASE_INSENSITIVE);

    private final static String INPUT_MAP =
        "Inputs:\n" +
        "    "+CANCEL+  " - Cancel\n" +
        "    "+INSERT+  " <amount> - Inserted coin\n" +
        "    "+SHELF+   " <shelf> - Selected shelf\n" +
        "    "+BALANCE+ " - Print cash balance\n" +
        "    "+PRODUCTS+" - Print shelf content\n" +
        "    "+QUIT+    " - Quit\n";

    private static void printInputMap()
    {
        print(INPUT_MAP);
    }

    private static void changeReturned(Coin change)
    {
        print("Returning change: "+change);
    }

    private static IntConsumer productDropped(ShelfInformation info)
    {
        return shelf -> print(info.infoForShelf(shelf)
            .map(i -> "Dropped: "+i.productType)
            .orElse("Dropped unknown product"));
    }

    private static void initShelfAndPrices(SimpleShelfInformation shelfInformation, SimplePrices prices)
    {
        ProductType cola = ProductType.withName("Cola");
        ProductType chocolateBar = ProductType.withName("Chocolate Bar");
        ProductType mineralWater = ProductType.withName("Mineral Water");

        shelfInformation.setInfo(0, ShelfContent.of(cola, 8));
        shelfInformation.setInfo(1, ShelfContent.of(chocolateBar, 2));
        shelfInformation.setInfo(5, ShelfContent.of(mineralWater, 10));

        prices.setPrice(cola, Money.whole(1));
        prices.setPrice(chocolateBar, Money.cents(40));
        prices.setPrice(mineralWater, Money.cents(30));
    }

    private static String extractArgument(String line, String symbol)
    {
        return line.replaceFirst("(?i)^"+symbol+"\\s*", "");
    }

    /**
     * @throws IOException Is actually never thrown.
     */
    private static void processUserInputs(VendingMachine machine, CashControllerImpl cash, ProductControllerImpl products) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String line;
        while((line = reader.readLine()) != null)
        {
            line = line.trim();

            if(line.isEmpty())
                continue;

            if(line.equalsIgnoreCase(QUIT))
            {
                break;
            }
            else if(line.equalsIgnoreCase(CANCEL))
            {
                machine.cancelSelected();
                continue;
            }
            else if(line.equalsIgnoreCase(BALANCE))
            {
                print("Cash balance:\n"+cash.balanceReport());
                continue;
            }
            else if(line.equalsIgnoreCase(PRODUCTS))
            {
                print("Shelf content:\n"+products.inventoryReport());
                continue;
            }
            else if(line.toUpperCase().startsWith(INSERT))
            {
                Matcher m = COIN_PATTERN.matcher(line);
                if(m.matches())
                {
                    String firstGroup = m.group(1);
                    String secondGroup = m.group(2);

                    //we assume that integer without trailing '.00' is a whole coin
                    int whole = firstGroup == null ? parseInt(secondGroup) : parseInt(firstGroup);
                    int cents = firstGroup == null ? 0 : parseInt(secondGroup);

                    //if user forgot trailing zeros
                    if(firstGroup != null && cents < 10)
                        cents *= 10;

                    Money money = Money.money(whole, cents);

                    Optional<Coin> maybeCoin = Coin.ofValue(money);
                    if(maybeCoin.isPresent())
                        maybeCoin.ifPresent(machine::coinInserted);
                    else
                        print("'"+money+"' is not a supported denomination.");

                    continue;
                }
                else
                {
                    String arg = extractArgument(line, INSERT);
                    print("'" + arg + "' is not a valid coin.");
                    continue;
                }
            }
            else if(line.toUpperCase().startsWith(SHELF))
            {
                Matcher m = SHELF_PATTERN.matcher(line);
                if(m.matches())
                {
                    String shelfGroup = m.group(1);

                    int shelf = parseInt(shelfGroup);
                    if(shelf > NUMBER_OF_SHELVES)
                        print("Machine does not have that many shelves.");
                    else if(shelf == 0)
                        print("Shelf numbers start from 1.");
                    else
                        machine.shelfSelected(shelf-1);
                    continue;
                }
                else
                {
                    String arg  = extractArgument(line, SHELF);
                    print("'"+arg+"' is not a valid shelf number.");
                }
            }

            print("Invalid input: '"+line+"'");
        }
    }

    private static void print(String message)
    {
        System.out.println(message);
        System.out.println();
    }

    public static void main(String[] args) throws IOException
    {
        CashHolder cashHolder = CashHolder.using(ConsoleSimulator::changeReturned);

        CashControllerImpl cash = CashControllerImpl.using(cashHolder);

        SimpleShelfInformation shelfInformation =
            SimpleShelfInformation.create();

        SimplePrices prices = SimplePrices.create();

        initShelfAndPrices(shelfInformation, prices);

        ProductHolder productHolder =
            ProductHolder.using(NUMBER_OF_SHELVES, SHELF_SIZE, productDropped(shelfInformation));

        ProductControllerImpl products =
            ProductControllerImpl.fromHolderInitializedWith(productHolder, shelfInformation);

        VendingMachine machine = VendingMachineBuilder
            .withDisplay(ConsoleSimulator::print)
            .using(cash)
            .using(products)
            .withPrices(prices);

        printInputMap();

        machine.start();

        processUserInputs(machine, cash, products);

        machine.stop();
    }
}
