package tdd.vending.hardware;

import tdd.vending.money.*;
import java.util.function.Consumer;

/**
 * Class representing the hardware actually holding all the coins.
 * The assumption is that coins simply drop into this holder without
 * the hardware being aware of the fact and propagating the information
 * upstream.
 *
 * The hardware can only release a specified coin. However it is not
 * responsible for knowing whether it has the actual coin or not.
 * This functionality should be provided by the <code>{@link CashController}</code>.
 * For the same reason the amount of cash available at the beginning
 * is not known and should be specified in the <code>{@link CashController}</code>
 * by the person servicing the machine.
 */
public interface CashHolder
{
    /**
     * After this method returns the coin specified in the argument
     * should be returned to the client.
     *
     * TODO Allow hardware to report error by throwing an exception
     *
     * @param coin Coin that should be released.
     */
    void returnChange(Coin coin);

    /**
     * Convenience method for easy creation of instances that
     * delegate functionality to the specified handler.
     *
     * @param delegate Object that actually handles releasing change.
     */
    static CashHolder using(Consumer<Coin> delegate)
    {
        return coin -> delegate.accept(coin);
    }
}
