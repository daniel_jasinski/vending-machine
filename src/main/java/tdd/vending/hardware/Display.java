package tdd.vending.hardware;

/**
 * Class representing hardware displaying messages to the user.
 */
public interface Display
{
    /**
     * Method should display specified message to the user.
     * Only one message at a time should be displayed, so each
     * subsequent message should override the previous one.
     *
     * @param message Message to be displayed
     */
    void showMessage(String message);
}
