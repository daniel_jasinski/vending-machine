package tdd.vending.hardware;

import java.util.function.IntConsumer;

/**
 * Class representing hardware responsible for releasing purchased
 * products to the user. This hardware is not ware of the content
 * of the shelves and only can perform a simple operation of releasing
 * an item from a specified shelf.
 */
public interface ProductHolder
{
    /**
     * @return Number of shelves available in this machine.
     */
    int numberOfShelves();

    /**
     * @return Maximum number of items that can be stored on a single shelf.
     */
    int shelfCapacity();

    /**
     * After this method returns product from the specified shelf
     * should have been released to the client.
     *
     * TODO Allow hardware to report error by throwing an exception
     *
     * @param shelf Shelf id (0-based) from which to return product.
     */
    void dropProductFromShelf(int shelf);

    /**
     * Convenience method for easy generating of implementations that
     * delegate functionality to the specified handler.
     *
     * @param numberOfShelves Number of shelves in the device
     * @param shelfCapacity Maximum number of items on a shelf
     * @param dropHandler Handler responsible for releasing a single product
     *                    from specified shelf.
     */
    static ProductHolder using(int numberOfShelves, int shelfCapacity, IntConsumer dropHandler)
    {
        if(numberOfShelves <= 0)
            throw new IllegalArgumentException("Number of shelves should be positive.");
        if(shelfCapacity <= 0)
            throw new IllegalArgumentException("Shelf capacity should be positive.");

        return new ProductHolder()
        {
            @Override public int numberOfShelves() { return numberOfShelves; }
            @Override public int shelfCapacity() { return shelfCapacity; }
            @Override public void dropProductFromShelf(int shelf) { dropHandler.accept(shelf); }
        };
    }
}
