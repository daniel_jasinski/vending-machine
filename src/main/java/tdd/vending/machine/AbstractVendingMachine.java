package tdd.vending.machine;

import tdd.vending.money.*;
import tdd.vending.products.*;
import java.util.Optional;

/**
 * Interface representing all the necessary functionality provided
 * by a vending machine so that it can correctly respond to the
 * user interactions. This interface is manipulated by a state
 * machine implemented in the <code>{@link VendingMachineState}</code> class.
 */
public interface AbstractVendingMachine
{
    boolean canReturnChange(Money money);
    void returnChange(Money money);

    boolean areShelvesEmpty();
    void dropProductFromShelf(int shelf);

    Optional<ProductType> productOnShelf(int shelf);
    Optional<Money> priceOf(ProductType productType);
}
