package tdd.vending.machine;

import tdd.vending.hardware.Display;
import tdd.vending.money.*;
import tdd.vending.products.*;
import java.util.Optional;

/**
 * Concrete implementation of the <code>{@link AbstractVendingMachine}</code> interface
 * that receives user interactions with the machine and passes it on
 * to appropriate underlying sub-components.
 */
public class VendingMachine implements AbstractVendingMachine, VendingMachineActionListener
{
    private final CashController cash;
    private final ProductController products;
    private final Prices prices;

    private final VendingMachineState state;

    VendingMachine(Display display, CashController cash, ProductController products, Prices prices)
    {
        this.cash = cash;
        this.products = products;
        this.prices = prices;

        this.state = VendingMachineState.using(this, display);
    }



    public void start()
    {
        state.start();
    }

    public void stop()
    {
        state.stop();
    }



    @Override public void coinInserted(Coin coin)
    {
        cash.coinInserted(coin);
        state.coinInserted(coin);
    }

    @Override public void shelfSelected(int shelf)
    {
        state.shelfSelected(shelf);
    }

    @Override public void cancelSelected()
    {
        state.cancelSelected();
    }



    @Override public void returnChange(Money money)
    {
        cash.returnChange(money);
    }

    @Override public boolean canReturnChange(Money money)
    {
        return cash.canReturnChange(money);
    }

    @Override public boolean areShelvesEmpty()
    {
        return products.areAllShelvesEmpty();
    }

    @Override public void dropProductFromShelf(int shelf)
    {
        products.dropProductFromShelf(shelf);
    }

    @Override public Optional<ProductType> productOnShelf(int shelf)
    {
        return products.productOnShelf(shelf);
    }

    @Override public Optional<Money> priceOf(ProductType productType)
    {
        return prices.priceOf(productType);
    }
}
