package tdd.vending.machine;

import tdd.vending.money.Coin;

/**
 * Interface representing all possible interactions
 * that a user can have with the vending machine.
 */
public interface VendingMachineActionListener
{
    /**
     * This method will be called when user inserts a coin into the machine.
     *
     * @param coin Coin inserted by the user.
     */
    void coinInserted(Coin coin);

    /**
     * This method will be called when user presses a shelf button.
     *
     * @param shelf 0-based id of the shelf user selected.
     */
    void shelfSelected(int shelf);

    /**
     * This method will be called when user clicks the <i>Cancel</i> button.
     */
    void cancelSelected();
}
