package tdd.vending.machine;

import tdd.vending.hardware.*;
import tdd.vending.money.*;
import tdd.vending.products.*;

/**
 * Convenience class for assembling a vending machine from
 * its sub-components and initial information.
 */
public class VendingMachineBuilder
{
    public static CashBuilder withDisplay(Display display)
    {
        return new CashBuilder(display);
    }

    public static class CashBuilder
    {
        private final Display display;

        private CashBuilder(Display display)
        {
            this.display = display;
        }

        public ProductsBuilder withMoneyIn(CashHolder holder)
        {
            return using(CashControllerImpl.using(holder));
        }

        public ProductsBuilder withMoneyIn(CashHolder holder, CashInformationProvider info)
        {
            return using(CashControllerImpl.usingInitializedWith(holder, info));
        }

        public ProductsBuilder using(CashController controller)
        {
            return new ProductsBuilder(display, controller);
        }
    }

    public static class ProductsBuilder
    {
        private final Display display;
        private final CashController cash;

        private ProductsBuilder(Display display, CashController cash)
        {
            this.display = display;
            this.cash = cash;
        }

        public PricesBuilder withProducts(ProductHolder holder, ShelfInformation info)
        {
            return using(ProductControllerImpl.fromHolderInitializedWith(holder, info));
        }

        public PricesBuilder using(ProductController products)
        {
            return new PricesBuilder(display, cash, products);
        }
    }

    public static class PricesBuilder
    {
        private final Display display;
        private final CashController cash;
        private final ProductController products;

        private PricesBuilder(Display display, CashController cash, ProductController products)
        {
            this.display = display;
            this.cash = cash;
            this.products = products;
        }

        public VendingMachine withPrices(Prices prices)
        {
            return new VendingMachine(display, cash, products, prices);
        }
    }
}
