package tdd.vending.machine;

import tdd.vending.hardware.Display;
import tdd.vending.machine.state.*;
import tdd.vending.money.Coin;
import java.util.concurrent.*;

import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Class responsible for manipulating the state
 * of an <code>{@link AbstractVendingMachine}</code>
 * in response to user actions.
 */
class VendingMachineState
{
    private ScheduledExecutorService executor;

    private volatile State state = State.dummy();

    private final AbstractVendingMachine machine;
    private final Display display;

    private VendingMachineState(AbstractVendingMachine machine, Display display)
    {
        this.machine = machine;
        this.display = display;
    }

    static VendingMachineState using(AbstractVendingMachine machine, Display display)
    {
        return new VendingMachineState(machine, display);
    }

    private boolean started()
    {
        return ! State.isDummy(state);
    }

    private void assureNotStarted()
    {
        if(started())
            throw new IllegalStateException("Machine already started.");
    }

    private void assureStarted()
    {
        if( ! started())
            throw new IllegalStateException("Machine was not started yet.");
    }

    void start()
    {
        assureNotStarted();
        executor = Executors.newSingleThreadScheduledExecutor();
        setNewState(State.initialStateFor(machine));
    }

    void stop()
    {
        assureStarted();
        state = State.dummy();
        executor.shutdown();
        executor = null;
    }

    synchronized
    private void setNewState(State state)
    {
        this.state = state;
        display.showMessage(state.userMessage());
    }

    synchronized
    private void setNewStateIfCurrent(State maybeCurrent, State newState)
    {
        if(this.state == maybeCurrent)
            setNewState(newState);
    }

    private void scheduleStateTransition(State temporaryState, State finalState, int duration)
    {
        setNewState(temporaryState);
        executor.schedule(() -> setNewStateIfCurrent(temporaryState, finalState), duration, SECONDS);
    }

    private void transition(StateTransition transition)
    {
        transition.whenNormal(this::setNewState);
        transition.whenTemporary(this::scheduleStateTransition);
    }

    void coinInserted(Coin coin)
    {
        assureStarted();
        transition(state.coinInserted(coin));
    }

    void shelfSelected(int shelf)
    {
        assureStarted();
        transition(state.shelfSelected(shelf));
    }

    void cancelSelected()
    {
        assureStarted();
        transition(state.cancelSelected());
    }
}
