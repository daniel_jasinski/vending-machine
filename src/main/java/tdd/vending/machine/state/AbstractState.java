package tdd.vending.machine.state;

import tdd.vending.machine.AbstractVendingMachine;
import tdd.vending.money.*;

abstract class AbstractState implements State
{
    protected final AbstractVendingMachine machine;
    private final String message;

    AbstractState(AbstractVendingMachine machine, String message)
    {
        this.machine = machine;
        this.message = message;
    }

    @Override public String userMessage()
    {
        return message;
    }

    @Override public StateTransition cancelSelected()
    {
        return StateTransition.none();
    }

    void returnChange(Money amount)
    {
        machine.returnChange(amount);
    }

    void returnCoin(Coin coin)
    {
        returnChange(coin.value());
    }

    StateTransition longDelayThenBackToThis(State state)
    {
        return StateTransition.temporaryWithLongDelay(state, this);
    }

    StateTransition defaultDelayThenBackToThis(State state)
    {
        return StateTransition.temporaryWithDefaultDelay(state, this);
    }

    StateTransition displayMessageThenBackToThis(String message)
    {
        return TransientState.defaultTransitionWith(machine, message, this);
    }

    StateTransition displayMessageThenSwitchTo(String message, State nextState)
    {
        return TransientState.defaultTransitionWith(machine, message, nextState);
    }
}
