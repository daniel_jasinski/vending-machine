package tdd.vending.machine.state;

import tdd.vending.money.Coin;

import static tdd.vending.machine.state.StateTransition.*;

public class DummyState implements State
{
    @Override public StateTransition coinInserted(Coin coin) { return none(); }
    @Override public StateTransition cancelSelected() { return none(); }
    @Override public StateTransition shelfSelected(int shelf) { return none(); }
    @Override public String userMessage() { return ""; }
}
