package tdd.vending.machine.state;

import tdd.vending.machine.AbstractVendingMachine;
import tdd.vending.money.*;

import static tdd.vending.machine.state.Messages.*;
import static tdd.vending.money.Money.ZERO;

public class InsertingCoins extends AbstractState
{
    private final int shelf;
    private final Money price;
    private final Money inserted;

    private InsertingCoins(AbstractVendingMachine machine, int shelf, Money price, Money inserted)
    {
        super(machine, AMOUNT_LEFT_TO_PAY_PREFIX + price.subtract(inserted));
        this.shelf = shelf;
        this.price = price;
        this.inserted = inserted;
    }

    static InsertingCoins into(AbstractVendingMachine machine, int shelf, Money price, Money inserted)
    {
        return new InsertingCoins(machine, shelf, price, inserted);
    }

    @Override public StateTransition coinInserted(Coin coin)
    {
        Money newTotal = inserted.add(coin.value());
        if(newTotal.isGreaterThan(price))
            return handleOverpaying(newTotal);
        else if(newTotal.isEqual(price))
            return releaseProduct();
        else
            return afterInserting(newTotal);
    }

    @Override public StateTransition cancelSelected()
    {
        if(inserted.isGreaterThan(ZERO))
            returnChange(inserted);
        return StateTransition.normal(WaitingForUserAction.on(machine));
    }

    @Override public StateTransition shelfSelected(int shelf)
    {
        return displayMessageThenBackToThis(Messages.SHELF_SELECTED_WHILE_INSERTING_COINS);
    }

    private void dropProductFromShelf()
    {
        machine.dropProductFromShelf(shelf);
    }

    private boolean canReturnChange(Money amount)
    {
        return machine.canReturnChange(amount);
    }

    private StateTransition afterInserting(Money totalInserted)
    {
        return StateTransition.normal(into(machine, shelf, price, totalInserted));
    }

    private StateTransition handleOverpaying(Money newTotal)
    {
        Money overpaid = newTotal.subtract(price);
        if(canReturnChange(overpaid))
        {
            returnChange(overpaid);
            return releaseProduct();
        }
        else
        {
            // Client just inserted this amount so we can always return it
            returnChange(newTotal);
            return displayMessageThenSwitchTo(NO_CHANGE_MESSAGE, WaitingForUserAction.on(machine));
        }
    }

    private StateTransition releaseProduct()
    {
        dropProductFromShelf();

        State nextState;
        if(machine.areShelvesEmpty())
            nextState = OutOfProducts.in(machine);
        else
            nextState = WaitingForUserAction.on(machine);

        return displayMessageThenSwitchTo(Messages.AFTER_PURCHASE, nextState);
    }
}
