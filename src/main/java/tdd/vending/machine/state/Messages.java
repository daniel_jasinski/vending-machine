package tdd.vending.machine.state;

public class Messages
{
    public final static String OUT_OF_PRODUCTS =
        "We are sorry but this machine is out of products.";

    public final static String AFTER_COIN_INSERTED_WHEN_OUT_OF_PRODUCTS =
        "This machine has no more products to sell!";

    public final static String WAITING_FOR_USER =
        "Please select a shelf number to display product information.";

    public final static String AFTER_COIN_INSERTED_WITHOUT_SELECTING_SHELF =
        "You must first select a shelf.";

    public final static String EMPTY_SHELF =
        "The selected shelf is empty.";

    public final static String PRICE_PREFIX =
        "Price: ";

    public final static String UNEXPECTED_ERROR =
        "Something went wrong.";

    public final static String PRICE_ERROR =
        "Cannot display price.";

    public final static String AMOUNT_LEFT_TO_PAY_PREFIX =
        "To pay: ";

    public final static String NO_CHANGE_MESSAGE =
        "We are sorry but this machine cannot give you an appropriate change.";

    public final static String SHELF_SELECTED_WHILE_INSERTING_COINS =
        "You are in the middle of a transaction.\nUse Cancel button to stop this operation and select a different product.";

    public final static String AFTER_PURCHASE =
        "Thank you for your purchase.\nHave a nice day!";
}
