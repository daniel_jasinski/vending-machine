package tdd.vending.machine.state;

import tdd.vending.machine.AbstractVendingMachine;
import tdd.vending.money.Coin;

class OutOfProducts extends AbstractState
{
    private OutOfProducts(AbstractVendingMachine machine)
    {
        super(machine, Messages.OUT_OF_PRODUCTS);
    }

    static OutOfProducts in(AbstractVendingMachine machine)
    {
        return new OutOfProducts(machine);
    }

    @Override public StateTransition shelfSelected(int shelf)
    {
        return displayMessageThenBackToThis(Messages.EMPTY_SHELF);
    }

    @Override public StateTransition coinInserted(Coin coin)
    {
        returnCoin(coin);
        return displayMessageThenBackToThis(Messages.AFTER_COIN_INSERTED_WHEN_OUT_OF_PRODUCTS);
    }
}
