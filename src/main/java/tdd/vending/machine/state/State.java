package tdd.vending.machine.state;

import tdd.vending.machine.AbstractVendingMachine;
import tdd.vending.money.Coin;

public interface State
{
    StateTransition coinInserted(Coin coin);
    StateTransition cancelSelected();
    StateTransition shelfSelected(int shelf);
    String userMessage();

    static State initialStateFor(AbstractVendingMachine machine)
    {
        if(machine.areShelvesEmpty())
            return OutOfProducts.in(machine);
        else
            return WaitingForUserAction.on(machine);
    }

    static boolean isDummy(State state)
    {
        return state instanceof DummyState;
    }

    static State dummy()
    {
        return new DummyState();
    }
}
