package tdd.vending.machine.state;

/**
 * Algebraic type holding information about whether the state transition
 * is normal or just temporary.
 */
public class StateTransition
{
    final static int DEFAULT_DELAY_SECONDS = 5;
    final static int LONG_DELAY_SECONDS = 20;

    final static StateTransition NONE = new StateTransition();

    /**
     * Method will call <code>handler</code> if this instance represents
     * a normal state transition.
     *
     * @param handler Object responsible for applying new state.
     */
    public void whenNormal(NormalTransitionHandler handler) { }

    /**
     * Method will call <code>handler</code> if this instance represents
     * a temporary state transition.
     * @param handler Object responsible for applying the new temporary state
     *                and then switching to a new state after specified delay.
     */
    public void whenTemporary(TemporaryTransitionHandler handler) { }

    /**
     * @return No state transition.
     */
    public static StateTransition none()
    {
        return NONE;
    }

    /**
     * @param newState New state that should be applied.
     * @return Normal state transition.
     */
    public static StateTransition normal(State newState)
    {
        return new Normal(newState);
    }

    /**
     * @param temporaryState New temporary state that should be applied.
     * @param finalState Final state that should be applied after specified number of seconds.
     * @param delaySeconds Temporary state duration in seconds.
     * @return Temporary state transition.
     * @throws IllegalArgumentException when delay is not positive
     */
    public static StateTransition temporary(State temporaryState, State finalState, int delaySeconds)
    {
        if(delaySeconds <= 0)
            throw new IllegalArgumentException("Delay has to be positive.");
        return new Temporary(temporaryState, finalState, delaySeconds);
    }

    /**
     * Temporary state transition after default delay.
     * @see StateTransition#temporary(State, State, int)
     */
    public static StateTransition temporaryWithDefaultDelay(State temporaryState, State finalState)
    {
        return temporary(temporaryState, finalState, DEFAULT_DELAY_SECONDS);
    }

    /**
     * Temporary state transition after long delay.
     * @see StateTransition#temporary(State, State, int)
     */
    public static StateTransition temporaryWithLongDelay(State temporaryState, State finalState)
    {
        return temporary(temporaryState, finalState, LONG_DELAY_SECONDS);
    }

    public interface NormalTransitionHandler
    {
        void accept(State nextState);
    }

    public interface TemporaryTransitionHandler
    {
        void accept(State temporaryState, State finalState, int temporarySecond);
    }

    private StateTransition() { }

    static class Normal extends StateTransition
    {
        final State nextState;

        private Normal(State nextState)
        {
            this.nextState = nextState;
        }

        @Override public void whenNormal(NormalTransitionHandler handler)
        {
            handler.accept(nextState);
        }
    }

    static class Temporary extends StateTransition
    {
        final State temporaryState;
        final State finalState;
        final int transitionSeconds;

        private Temporary(State temporaryState, State finalState, int transitionSeconds)
        {
            this.temporaryState = temporaryState;
            this.finalState = finalState;
            this.transitionSeconds = transitionSeconds;
        }

        @Override public void whenTemporary(TemporaryTransitionHandler handler)
        {
            handler.accept(temporaryState, finalState, transitionSeconds);
        }
    }
}
