package tdd.vending.machine.state;

import tdd.vending.machine.AbstractVendingMachine;
import tdd.vending.money.Coin;

class TransientState extends AbstractState
{
    protected final State fallback;

    TransientState(AbstractVendingMachine machine, String message, State fallback)
    {
        super(machine, message);
        this.fallback = fallback;
    }

    static TransientState with(AbstractVendingMachine machine, String message, State fallback)
    {
        return new TransientState(machine, message, fallback);
    }

    static StateTransition defaultTransitionWith(AbstractVendingMachine machine, String message, State fallback)
    {
        return StateTransition.temporaryWithDefaultDelay(with(machine, message, fallback), fallback);
    }

    State getFallback()
    {
        return fallback;
    }

    @Override public StateTransition coinInserted(Coin coin)
    {
        return fallback.coinInserted(coin);
    }

    @Override public StateTransition cancelSelected()
    {
        return StateTransition.normal(fallback);
    }

    @Override public StateTransition shelfSelected(int shelf)
    {
        return fallback.shelfSelected(shelf);
    }
}
