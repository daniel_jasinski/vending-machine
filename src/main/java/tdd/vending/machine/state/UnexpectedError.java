package tdd.vending.machine.state;

import tdd.vending.machine.AbstractVendingMachine;

class UnexpectedError extends TransientState
{
    private UnexpectedError(AbstractVendingMachine machine, String details, State fallback)
    {
        super(machine, Messages.UNEXPECTED_ERROR+"\n"+details, fallback);
    }

    static UnexpectedError with(AbstractVendingMachine machine, String details, State fallback)
    {
        return new UnexpectedError(machine, details, fallback);
    }

    @Override public StateTransition shelfSelected(int shelf)
    {
        return StateTransition.normal(fallback);
    }
}
