package tdd.vending.machine.state;

import tdd.vending.machine.AbstractVendingMachine;
import tdd.vending.money.*;
import tdd.vending.products.ProductType;

import static tdd.vending.machine.state.StateTransition.*;

class WaitingForFirstCoin extends WaitingForUserAction
{
    private final int shelf;
    private final Money price;

    private WaitingForFirstCoin(AbstractVendingMachine machine, int shelf, ProductType product, Money price)
    {
        super(machine, product+"\n"+Messages.PRICE_PREFIX+price);
        this.shelf = shelf;
        this.price = price;
    }

    public static WaitingForFirstCoin in(AbstractVendingMachine machine, int shelf, ProductType product, Money price)
    {
        return new WaitingForFirstCoin(machine, shelf, product, price);
    }

    @Override public StateTransition coinInserted(Coin coin)
    {
        // delegate here is necessary to handle cases when single coin
        // is equal to or exceeds the price
        InsertingCoins delegate = InsertingCoins.into(machine, shelf, price, Money.ZERO);
        return delegate.coinInserted(coin);
    }

    @Override public StateTransition cancelSelected()
    {
        return normal(WaitingForUserAction.on(machine));
    }
}
