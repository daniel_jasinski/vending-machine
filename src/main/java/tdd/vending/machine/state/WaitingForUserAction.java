package tdd.vending.machine.state;

import tdd.vending.machine.AbstractVendingMachine;
import tdd.vending.money.*;
import tdd.vending.products.ProductType;

class WaitingForUserAction extends AbstractState
{
    WaitingForUserAction(AbstractVendingMachine machine)
    {
        this(machine, Messages.WAITING_FOR_USER);
    }

    WaitingForUserAction(AbstractVendingMachine machine, String message)
    {
        super(machine, message);
    }

    static WaitingForUserAction on(AbstractVendingMachine machine)
    {
        return new WaitingForUserAction(machine);
    }

    @Override public StateTransition coinInserted(Coin coin)
    {
        returnCoin(coin);
        return displayMessageThenBackToThis(Messages.AFTER_COIN_INSERTED_WITHOUT_SELECTING_SHELF);
    }

    @Override public StateTransition shelfSelected(int shelf)
    {
        return machine.productOnShelf(shelf)
            .map(product -> displayProductInfo(shelf, product))
            .orElseGet(this::displayEmptyShelfInfo);
    }

    private StateTransition displayProductInfo(int shelf, ProductType product)
    {
        return machine.priceOf(product)
            .map(price -> waitForFirstCoin(shelf, product, price))
            .orElseGet(this::displayPriceError);
    }

    private StateTransition waitForFirstCoin(int shelf, ProductType product, Money price)
    {
        return longDelayThenBackToThis(WaitingForFirstCoin.in(machine,shelf,product,price));
    }

    private StateTransition displayPriceError()
    {
        return defaultDelayThenBackToThis(UnexpectedError.with(machine, Messages.PRICE_ERROR,this));
    }

    private StateTransition displayEmptyShelfInfo()
    {
        return displayMessageThenBackToThis(Messages.EMPTY_SHELF);
    }
}
