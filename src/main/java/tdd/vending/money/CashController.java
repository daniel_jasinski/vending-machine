package tdd.vending.money;

/**
 * Interface responsible for keeping track of the money
 * available in the machine. It is also responsible for
 * telling the actual hardware to release necessary change.
 */
public interface CashController
{
    Money totalAmount();
    int numberOf(Coin coin);

    boolean canReturnChange(Money amount);
    void returnChange(Money amount);

    /**
     * Method should call underlying hardware to release specified coin.
     */
    void coinInserted(Coin coin);
}
