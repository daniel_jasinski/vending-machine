package tdd.vending.money;

import tdd.vending.hardware.CashHolder;
import java.util.*;
import java.util.Map.Entry;

/**
 * Basic implementation of the <code>{@link CashController}</code> interface.
 */
public final class CashControllerImpl implements CashController
{
    private final Map<Coin, Integer> money = new EnumMap<>(Coin.class);
    private final CashHolder bank;

    private CashControllerImpl(CashHolder bank)
    {
        this.bank = bank;
        reset();
    }

    public static CashControllerImpl using(CashHolder bank)
    {
        return new CashControllerImpl(bank);
    }

    public static CashControllerImpl usingInitializedWith(CashHolder bank, CashInformationProvider info)
    {
        CashControllerImpl instance = using(bank);
        instance.setValues(info);
        return instance;
    }

    public void reset()
    {
        resetWith(CashInformationProvider.noMoney());
    }

    public void resetWith(CashInformationProvider initial)
    {
        setValues(initial);
    }

    private void setValues(CashInformationProvider info)
    {
        for(Coin coin: Coin.values())
            money.put(coin, info.numberOf(coin));
    }

    @Override public void coinInserted(Coin c)
    {
        Integer newAmount = money.get(c) + 1;
        money.put(c, newAmount);
    }

    @Override public Money totalAmount()
    {
        Money sum = Money.ZERO;
        for(Entry<Coin, Integer> entry: money.entrySet())
        {
            Coin coin = entry.getKey();
            int n = entry.getValue();
            sum = sum.add(coin.value().times(n));
        }
        return  sum;
    }

    @Override public int numberOf(Coin coin)
    {
        return money.get(coin);
    }

    @Override public boolean canReturnChange(Money amount)
    {
        if(amount.isLessThan(Money.ZERO))
            throw new IllegalArgumentException("Cannot retrieve negative amount.");

        Money moneyLeft = amount;
        for(Coin coin: Coin.values())
        {
            int numberOfThisCoins = possibleNumberOfCoinsFor(coin, moneyLeft);
            moneyLeft = moneyLeft.subtract(coin.value().times(numberOfThisCoins));
        }
        return moneyLeft.isEqual(Money.ZERO);
    }

    @Override public void returnChange(Money amount)
    {
        if( ! canReturnChange(amount))
            throw new IllegalStateException("Cannot retrieve: "+amount);

        Money moneyLeft = amount;
        for(Coin coin: Coin.values())
        {
            int numberOfThisCoins = possibleNumberOfCoinsFor(coin, moneyLeft);
            moneyLeft = moneyLeft.subtract(coin.value().times(numberOfThisCoins));
            Integer newCoinCount = money.get(coin) - numberOfThisCoins;
            money.put(coin, newCoinCount);
            for(int i = 0; i < numberOfThisCoins; i++)
                bank.returnChange(coin);
        }
    }

    private int possibleNumberOfCoinsFor(Coin c, Money amount)
    {
        int coinsLeft = money.get(c);
        Money value = c.value();
        int coinsInTheAmount = value.numberOfTimesContainedIn(amount);
        return Math.min(coinsLeft, coinsInTheAmount);
    }

    public String balanceReport()
    {
        StringBuilder sb = new StringBuilder();

        Money sum = Money.ZERO;
        for(Entry<Coin, Integer> entry: money.entrySet())
        {
            Coin coin = entry.getKey();
            Money value = coin.value();
            int n = entry.getValue();
            sum = sum.add(value.times(n));
            sb.append(String.format("%-6s %d", coin+":", n)).append("\n");
        }
        sb.append("Total: ").append(sum);
        return sb.toString();
    }
}
