package tdd.vending.money;

/**
 * Interface providing the information about the initial amount of money
 * available in the machine. This information needs to be provided during
 * initialization because in the current implementation the hardware is
 * not aware of the amount of money it is holding.
 *
 * Typically this information should be provided by a person servicing
 * the machine after they changed the amount of money in the machine.
 */
public interface CashInformationProvider
{
    int numberOf(Coin coin);

    static CashInformationProvider noMoney()
    {
        return coin -> 0;
    }
}
