package tdd.vending.money;

import java.util.Optional;

public enum Coin
{
    FIVE(Money.whole(5)),
    TWO(Money.whole(2)),
    ONE(Money.whole(1)),
    FIFTY_CENTS(Money.cents(50)),
    TWENTY_CENTS(Money.cents(20)),
    TEN_CENTS(Money.cents(10))
    ;

    private final Money value;

    Coin(Money value)
    {
        this.value = value;
    }

    public Money value()
    {
        return value;
    }

    @Override public String toString()
    {
        return value.toString();
    }

    public static Optional<Coin> ofValue(Money money)
    {
        for(Coin coin: Coin.values())
        {
            if(coin.value().isEqual(money))
                return Optional.of(coin);
        }
        return Optional.empty();
    }
}
