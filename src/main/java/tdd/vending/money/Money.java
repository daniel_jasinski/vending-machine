package tdd.vending.money;

/**
 * Simple class for performing monetary calculations.
 */
public final class Money implements Comparable<Money>
{
    public final static Money ZERO = new Money(0);

    private final int cents;

    private Money(int cents)
    {
        this.cents = cents;
    }

    /**
     * Method returns the specified amount of cents.
     *
     * <code>
     *     Money m = Money.cents(40);
     *     // prints <b>0.40</b>
     *     System.out.println(m);
     * </code>
     *
     * This method <b>does</b> allow negative amounts.
     * This method <b>does not</b> allow number of cents to be greater than or equal to 100,
     * which would typically be a programmer error who inserted additional zeros.
     */
    public static Money cents(int cents)
    {
        if(Math.abs(cents) >= 100)
            throwCentsException();
        return new Money(cents);
    }

    /**
     * Method returns the specified amount of full monetary units.
     *
     * <code>
     *     Money m = Money.whole(7);
     *     // prints <b>7.00</b>
     *     System.out.println(m);
     * </code>
     *
     * This method <b>does</b> allow negative amounts.
     */
    public static Money whole(int units)
    {
        return new Money(100*units);
    }

    /**
     * Method returns the specified amount of full monetary units and cents.
     *
     * <code>
     *     Money m = Money.money(1, 30);
     *     // prints <b>1.30</b>
     *     System.out.println(m);
     * </code>
     *
     * This method <b>does not</b> allow negative amounts. To get negative amount
     * use <code>{@link Money#negate()}</code>
     * This method <b>does not</b> allow number of cents to be greater than or equal to 100,
     * which would typically be a programmer error who inserted additional zeros.
     */
    public static Money money(int units, int cents)
    {
        if(units < 0 || cents < 0)
            throw new IllegalArgumentException("Amount of money should be positive.");
        if(cents >= 100)
            throwCentsException();
        return new Money(100*units + cents);
    }

    private static void throwCentsException()
    {
        throw new IllegalArgumentException("Number of cents should be smaller than 100.");
    }

    public Money times(int times)
    {
        return new Money(times*cents);
    }

    public Money add(Money other)
    {
        return new Money(cents+other.cents);
    }

    public Money subtract(Money other)
    {
        return new Money(cents - other.cents);
    }

    public Money negate()
    {
        return new Money(-cents);
    }

    public static boolean areEqual(Money m1, Money m2)
    {
        return m1.cents == m2.cents;
    }

    public int numberOfTimesContainedIn(Money other)
    {
        return other.cents / cents;
    }

    public boolean isEqual(Money other)
    {
        return areEqual(this, other);
    }

    public boolean isGreaterThan(Money other)
    {
        return compareTo(other) > 0;
    }

    public boolean isGreaterOrEqual(Money other)
    {
        return compareTo(other) >= 0;
    }

    public boolean isLessThan(Money other)
    {
        return compareTo(other) < 0;
    }

    public boolean isLessOrEqual(Money other)
    {
        return compareTo(other) <= 0;
    }

    @Override public String toString()
    {
        return String.format("%d.%02d", cents / 100, cents % 100);
    }

    @Override public int compareTo(Money o)
    {
        if(cents < o.cents)
            return -1;
        else if(cents > o.cents)
            return 1;
        else
            return 0;
    }

    @Override public boolean equals(Object o)
    {
        if(this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;

        Money money = (Money) o;

        return isEqual(money);
    }

    @Override public int hashCode()
    {
        return cents;
    }
}
