package tdd.vending.products;

import tdd.vending.money.Money;
import java.util.Optional;

/**
 * Interface responsible for providing the information
 * about the price of each product.
 *
 * @see SimplePrices
 */
public interface Prices
{
    /**
     * Method returns Optional instance that may or may not contain price.
     * Optional return type is used here because the price database might
     * not recognize the specified product.
     *
     * TODO >> When the application is modified in such a way that a missing price
     *      >> is impossible, this method should be replaced by a method returning
     *      >> an instance of <code>{@link Money}</code>.
     *
     * @param productType Product for which to retrieve price.
     * @return <code>{@link Optional#empty()}</code> if <code>productType</code> is not recognized,
     * or <code>{@link Optional}</code> containing the actual price of the product.
     */
    Optional<Money> priceOf(ProductType productType);
}
