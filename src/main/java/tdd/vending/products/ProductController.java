package tdd.vending.products;

import java.util.Optional;

/**
 * Interface responsible for keeping track of the products
 * in the vending machine. It is also responsible for telling
 * the actual hardware to release a product from a given shelf.
 */
public interface ProductController
{
    int numberOfShelves();
    Optional<ProductType> productOnShelf(int shelf);
    int numberOfProductsOnShelf(int shelf);
    default boolean isShelfEmpty(int shelf)
    {
        return numberOfProductsOnShelf(shelf) == 0;
    }

    boolean areAllShelvesEmpty();

    /**
     * This method should notify the actual hardware to release
     * a product from the given shelf.
     */
    void dropProductFromShelf(int shelf);
}
