package tdd.vending.products;

import tdd.vending.hardware.ProductHolder;
import java.util.*;

/**
 * Basic implementation of the <code>{@link ProductController}</code> interface.
 */
public class ProductControllerImpl implements ProductController
{
    private final ProductHolder holder;
    private final List<Shelf> shelves;

    private ProductControllerImpl(ProductHolder holder)
    {
        this.holder = holder;
        shelves = new ArrayList<>(holder.numberOfShelves());
        for(int i = 0; i < holder.numberOfShelves(); i++)
            shelves.add(Shelf.ofSize(holder.shelfCapacity()));
    }

    static ProductControllerImpl fromHolder(ProductHolder holder)
    {
        return new ProductControllerImpl(holder);
    }

    public static ProductControllerImpl fromHolderInitializedWith(ProductHolder holder, ShelfInformation information)
    {
        ProductControllerImpl instance = fromHolder(holder);
        instance.resetTo(information);
        return instance;
    }

    void resetTo(ShelfInformation information)
    {
        for(int i = 0; i < shelves.size(); i++)
        {
            Shelf shelf = shelves.get(i);
            information.infoForShelf(i)
                .ifPresent(info -> initShelfWith(shelf, info));
        }
    }

    private void initShelfWith(Shelf shelf, ShelfContent info)
    {
        shelf.clean();
        shelf.addItems(info.numberOfItems, info.productType);
    }

    private void validateShelfIndex(int shelf)
    {
        if(shelf < 0 || shelf >= shelves.size())
            throw new IllegalArgumentException("Shelf index: "+shelf+" is out of range.");
    }

    @Override public boolean areAllShelvesEmpty()
    {
        for(Shelf shelf: shelves)
            if( ! shelf.isEmpty())
                return false;
        return true;
    }

    @Override public int numberOfShelves()
    {
        return shelves.size();
    }

    @Override public int numberOfProductsOnShelf(int shelf)
    {
        validateShelfIndex(shelf);
        return shelves.get(shelf).numberOfItems();
    }

    @Override public Optional<ProductType> productOnShelf(int shelf)
    {
        validateShelfIndex(shelf);
        return shelves.get(shelf).productType();
    }

    @Override public void dropProductFromShelf(int shelf)
    {
        validateShelfIndex(shelf);
        shelves.get(shelf).removedItem();
        holder.dropProductFromShelf(shelf);
    }

    public String inventoryReport()
    {
        StringBuilder sb = new StringBuilder();
        for(int i = 1; i <= shelves.size(); i++)
            sb.append(String.format("%-3s ",i+":")).append(shelves.get(i-1).info()).append("\n");
        return sb.toString();
    }
}
