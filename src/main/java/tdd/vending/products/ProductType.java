package tdd.vending.products;

/**
 * Simple class representing a product available in the machine.
 * At the moment the only interesting feature of a product is its name.
 */
public final class ProductType
{
    private final String name;

    private ProductType(String name)
    {
        this.name = name;
    }

    public static ProductType withName(String name)
    {
        return new ProductType(name);
    }

    public String getName()
    {
        return name;
    }

    @Override public String toString()
    {
        return getName();
    }

    @Override public boolean equals(Object o)
    {
        if(this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;

        ProductType that = (ProductType) o;

        return name.equals(that.name);
    }

    @Override public int hashCode()
    {
        return name.hashCode();
    }
}
