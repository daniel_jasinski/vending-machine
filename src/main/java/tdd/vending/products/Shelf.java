package tdd.vending.products;

import java.util.Optional;

/**
 * Simple class for keeping track of the content of a shelf.
 * Provides information about the type of the product
 * on the shelf and the number of items.
 */
class Shelf
{
    private final int size;

    private ProductType productType;
    private int numberOfItems;

    private Shelf(int size)
    {
        this.size = size;
    }

    static Shelf ofSize(int size)
    {
        if(size <= 0)
            throw new IllegalArgumentException("Shelf size must be positive.");
        return new Shelf(size);
    }

    void clean()
    {
        numberOfItems = 0;
        productType = null;
    }

    void addItem(ProductType type)
    {
        addItems(1, type);
    }

    void addItems(int n, ProductType type)
    {
        if(this.productType != null && ! this.productType.equals(type))
            throw new IllegalArgumentException("Shelf can contain only one type of products.");
        int newNumberOfItems = numberOfItems + n;
        if(newNumberOfItems > size)
            throw new IllegalArgumentException("Shelf cannot hold specified number of items.");
        numberOfItems = newNumberOfItems;
        productType = type;
    }

    void removedItem()
    {
        if(isEmpty())
            throw new IllegalStateException("Cannot remove an item from an empty shelf.");
        numberOfItems--;
        if(isEmpty())
            productType = null;
    }

    Optional<ProductType> productType()
    {
        return Optional.ofNullable(productType);
    }

    int numberOfItems()
    {
        return numberOfItems;
    }

    boolean isEmpty()
    {
        return numberOfItems == 0;
    }

    String info()
    {
        if(isEmpty())
            return "empty";
        else
            return productType+" x "+numberOfItems;
    }

    @Override public String toString()
    {
        return info();
    }
}
