package tdd.vending.products;

/**
 * Simple data class containing information about the type
 * and number of items on a shelf.
 */
public class ShelfContent
{
    public final ProductType productType;
    public final int numberOfItems;

    private ShelfContent(ProductType productType, int numberOfItems)
    {
        this.productType = productType;
        this.numberOfItems = numberOfItems;
    }

    public static ShelfContent of(ProductType productType, int numberOfItems)
    {
        if(numberOfItems < 0)
            throw new IllegalArgumentException("Number of items cannot be negative.");
        return new ShelfContent(productType, numberOfItems);
    }
}
