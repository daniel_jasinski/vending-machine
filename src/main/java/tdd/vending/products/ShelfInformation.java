package tdd.vending.products;

import java.util.Optional;

/**
 * Interface providing the information about the initial content of the shelves.
 * This interface is necessary because <code>{@link ProductController}</code>
 * cannot retrieve information about the state of the shelves from the hardware
 * and requires this information to be specified during initialization.
 *
 * Typically this information should be provided by a person servicing
 * the machine after they changed the content of the shelves.
 *
 * @see SimpleShelfInformation
 */
public interface ShelfInformation
{
    /**
     * Method returns an instance of <code>{@link Optional}</code> containing
     * <code>{@link ShelfContent}</code> object, or <code>{@link Optional#empty()}</code>
     * if the shelf is empty or the shelf index is invalid.
     *
     * @param shelf 0-based index of the shelf
     */
    Optional<ShelfContent> infoForShelf(int shelf);
}
