package tdd.vending.products;

import tdd.vending.money.Money;
import java.util.*;

import static tdd.vending.money.Money.ZERO;

/**
 * Simple implementation of the <code>{@link Prices}</code> interface
 * that allows for an easy definition of the price for each product.
 *
 * @see SimplePrices#setPrice(ProductType, Money)
 */
public class SimplePrices implements Prices
{
    private final Map<ProductType, Money> prices = new HashMap<>();

    private SimplePrices() { }

    public static SimplePrices create()
    {
        return new SimplePrices();
    }

    /**
     * Method adds price for the specified product to the database.
     */
    public void setPrice(ProductType productType, Money amount)
    {
        if(amount.isLessOrEqual(ZERO))
            throw new IllegalArgumentException("Prices can only be positive");
        prices.put(productType, amount);
    }

    @Override public Optional<Money> priceOf(ProductType productType)
    {
        return Optional.ofNullable(prices.get(productType));
    }
}
