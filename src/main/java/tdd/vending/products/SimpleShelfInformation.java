package tdd.vending.products;

import java.util.*;

/**
 * Simple implementation of the <code>{@link ShelfInformation}</code> interface
 * that allows for an easy definition of the content of each shelf.
 *
 * @see SimpleShelfInformation#setInfo(int, ShelfContent)
 */
public class SimpleShelfInformation implements ShelfInformation
{
    private final Map<Integer, ShelfContent> infos = new HashMap<>();

    private SimpleShelfInformation() { }

    public static SimpleShelfInformation create()
    {
        return new SimpleShelfInformation();
    }

    /**
     * Method specifies content of a given shelf.
     *
     * @param shelf 0-based index of a shelf
     * @param info Information about the content of a given shelf.
     */
    public void setInfo(int shelf, ShelfContent info)
    {
        infos.put(shelf, info);
    }

    @Override public Optional<ShelfContent> infoForShelf(int shelf)
    {
        return Optional.ofNullable(infos.get(shelf));
    }
}
