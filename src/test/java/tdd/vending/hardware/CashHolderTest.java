package tdd.vending.hardware;

import org.junit.Test;
import tdd.vending.money.Coin;
import java.util.function.Consumer;

import static org.mockito.Mockito.*;

public class CashHolderTest
{
    @Test
    @SuppressWarnings("unchecked")
    public void using_creates_an_instance_that_delegates_functionality_to_the_supplied_handler()
    {
        Consumer<Coin> handler = mock(Consumer.class);
        Coin coin = Coin.TWENTY_CENTS;

        CashHolder holder = CashHolder.using(handler);

        holder.returnChange(coin);

        verify(handler).accept(coin);
    }
}