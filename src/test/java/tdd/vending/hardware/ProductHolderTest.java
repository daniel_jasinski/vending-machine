package tdd.vending.hardware;

import org.junit.Test;
import java.util.function.IntConsumer;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;
import static tdd.vending.hardware.ProductHolder.using;

public class ProductHolderTest
{
    @Test
    public void using_throw_IllegalArgumentException_when_number_of_shelves_is_not_positive()
    {
        assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> using(0, 1, mock(IntConsumer.class)));
        assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> using(-1, 1, mock(IntConsumer.class)));
    }

    @Test
    public void using_throw_IllegalArgumentException_when_shelf_capacity_is_not_positive()
    {
        assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> using(1, 0, mock(IntConsumer.class)));
        assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> using(1, -1, mock(IntConsumer.class)));
    }

    @Test
    public void using_returns_an_instance_delegating_all_methods_to_the_supplied_arguments()
    {
        int numberOfShelves = 7;
        int shelfCapacity = 9;
        int shelfToUse = 1;
        IntConsumer handler = mock(IntConsumer.class);

        ProductHolder holder = using(numberOfShelves, shelfCapacity, handler);
        holder.dropProductFromShelf(shelfToUse);

        assertThat(holder.numberOfShelves()).isEqualTo(numberOfShelves);
        assertThat(holder.shelfCapacity()).isEqualTo(shelfCapacity);
        verify(handler).accept(shelfToUse);
    }
}