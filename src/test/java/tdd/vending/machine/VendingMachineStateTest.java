package tdd.vending.machine;

import org.junit.Test;
import org.mockito.Mockito;
import tdd.vending.hardware.Display;
import tdd.vending.money.Coin;

import static org.assertj.core.api.Assertions.*;

public class VendingMachineStateTest
{
    @Test
    public void start_throws_IllegalStateException_when_called_second_time()
    {
        VendingMachineState state = buildState();

        state.start();

        assertThatExceptionOfType(IllegalStateException.class)
            .isThrownBy(state::start);
    }

    @Test
    public void stop_throws_IllegalStateException_when_called_before_start()
    {
        VendingMachineState state = buildState();

        assertThatExceptionOfType(IllegalStateException.class)
            .isThrownBy(state::stop);
    }

    @Test public void coinInserted_throws_IllegalStateException_when_called_before_start_is_called()
    {
        VendingMachineState state = buildState();

        assertThatExceptionOfType(IllegalStateException.class)
            .isThrownBy(() -> state.coinInserted(Coin.TEN_CENTS));
    }

    @Test
    public void shelfSelected_throws_IllegalStateException_when_called_before_start_is_called()
    {
        VendingMachineState state = buildState();

        assertThatExceptionOfType(IllegalStateException.class)
            .isThrownBy(() -> state.shelfSelected(0));
    }

    @Test
    public void cancelSelected_throws_IllegalStateException_when_called_before_start_is_called()
    {
        VendingMachineState state = buildState();

        assertThatExceptionOfType(IllegalStateException.class)
            .isThrownBy(state::cancelSelected);
    }

    private VendingMachineState buildState()
    {
        Display display = Mockito.mock(Display.class);
        AbstractVendingMachine machine = Mockito.mock(AbstractVendingMachine.class);
        return VendingMachineState.using(machine,display);
    }
}