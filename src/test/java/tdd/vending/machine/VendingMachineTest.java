package tdd.vending.machine;

import org.junit.*;
import org.junit.runner.RunWith;
import tdd.vending.hardware.*;
import tdd.vending.machine.state.Messages;
import tdd.vending.money.*;
import tdd.vending.products.*;
import tdd.vending.util.NestedRunner;
import java.util.function.Consumer;

import static org.mockito.Mockito.*;

@RunWith(NestedRunner.class)
public class VendingMachineTest
{
    private static void machineTest
    (
        Consumer<MachineSetup> preparator,
        Consumer<VendingMachine> operator,
        Consumer<MachineSetup> verifier
    )
    {
        MachineSetup setup = new MachineSetup();

        preparator.accept(setup);

        VendingMachine machine = setup.buildMachine();

        machine.start();

        operator.accept(machine);

        try
        {
            verifier.accept(setup);
        }
        finally
        {
            machine.stop();
        }
    }

    // preparators
    private static Consumer<MachineSetup> noPreparation()
    {
        return setup -> {};
    }

    private static Consumer<MachineSetup> productOnShelf(int shelf, ProductType product, int amount, Money price)
    {
        return setup ->
        {
            ShelfContent info = ShelfContent.of(product, amount);
            setup.productInfo.setInfo(shelf, info);
            setup.prices.setPrice(product, price);
        };
    }


    // operators
    private static Consumer<VendingMachine> coinInserted(Coin coin)
    {
        return machine -> machine.coinInserted(coin);
    }

    private static Consumer<VendingMachine> cancelPressed()
    {
        return VendingMachine::cancelSelected;
    }

    private static Consumer<VendingMachine> shelfSelected(int shelf)
    {
        return machine -> machine.shelfSelected(shelf);
    }


    // verifiers
    private static Consumer<MachineSetup> nothingHappened()
    {
        return noCoinWasReturned()
            .andThen(noProductWasReleased());
    }

    private static Consumer<MachineSetup> coinWasReturned(Coin coin)
    {
        return setup -> verify(setup.bank).returnChange(coin);
    }

    private static Consumer<MachineSetup> noCoinWasReturned()
    {
        return setup -> verify(setup.bank, never()).returnChange(any());
    }

    private static Consumer<MachineSetup> productWasReleased(int shelf)
    {
        return setup -> verify(setup.products).dropProductFromShelf(shelf);
    }

    private static Consumer<MachineSetup> noProductWasReleased()
    {
        return setup -> verify(setup.products, never()).dropProductFromShelf(anyInt());
    }

    private static Consumer<MachineSetup> messageWasDisplayed(String message)
    {
        return setup -> verify(setup.display).showMessage(message);
    }

    private static Consumer<MachineSetup> messageContainingTextWasDisplayed(String text)
    {
        return setup -> verify(setup.display).showMessage(contains(text));
    }



    public static class when_machine_has_no_products
    {
        private static OperationBuilder withNoProducts()
        {
            return with(noPreparation());
        }

        public static class after_coin_is_inserted
        {
            private final Coin coin = Coin.TEN_CENTS;

            private TestBuilder insertCoinAnd()
            {
                return withNoProducts().after(coinInserted(coin));
            }

            @Test public void the_coin_is_returned()
            {
                insertCoinAnd().assertThat(coinWasReturned(coin));
            }

            @Test public void no_product_is_released()
            {
                insertCoinAnd().assertThat(noProductWasReleased());
            }

            @Test public void warning_is_displayed()
            {
                insertCoinAnd().assertThat(messageWasDisplayed(Messages.AFTER_COIN_INSERTED_WHEN_OUT_OF_PRODUCTS));
            }
        }

        public static class after_cancel_is_pressed
        {
            @Test public void nothing_happens()
            {
                withNoProducts()
                    .after(cancelPressed())
                    .assertThat(nothingHappened());
            }
        }

        public static class after_shelf_is_selected
        {
            @Test public void nothing_happens()
            {
                withNoProducts()
                    .after(shelfSelected(0))
                    .assertThat(nothingHappened());
            }
        }
    }



    public static class when_machine_has_a_single_product_on_shelf
    {
        private final static int shelf = 0;
        private final static ProductType product = ProductType.withName("Cola");
        private final static int numberOfProducts = 1;
        private final static Money price = Money.whole(1);
        private final static Coin enough = Coin.ONE;
        private final static Coin tooLittle = Coin.TWENTY_CENTS;
        private final static Coin tooMuch = Coin.TWO;

        private static OperationBuilder withProductOnShelf()
        {
            return with(productOnShelf(shelf, product, numberOfProducts, price));
        }

        public static class after_coin_is_inserted
        {
            private final Coin coin = Coin.TEN_CENTS;

            private TestBuilder insertCoinAnd()
            {
                return withProductOnShelf().after(coinInserted(coin));
            }

            @Test public void the_coin_is_returned()
            {
                insertCoinAnd().assertThat(coinWasReturned(coin));
            }

            @Test public void no_product_is_released()
            {
                insertCoinAnd().assertThat(noProductWasReleased());
            }

            @Test public void warning_is_displayed()
            {
                insertCoinAnd().assertThat(messageWasDisplayed(Messages.AFTER_COIN_INSERTED_WITHOUT_SELECTING_SHELF));
            }
        }

        public static class after_cancel_is_pressed
        {
            @Test public void nothing_happens()
            {
                withProductOnShelf()
                    .after(cancelPressed())
                    .assertThat(nothingHappened());
            }
        }


        public static class after_shelf_is_selected
        {
            private static TestBuilder shelfWithProductsSelected()
            {
                return withProductOnShelf().after(shelfSelected(shelf));
            }

            @Test public void price_is_displayed()
            {
                shelfWithProductsSelected()
                    .assertThat(messageContainingTextWasDisplayed(price.toString()));
            }

            public static class and_enough_money_is_inserted
            {
                private TestBuilder enoughMoneyIsInserted()
                {
                    return shelfWithProductsSelected().and(coinInserted(enough));
                }

                @Test public void then_product_is_released()
                {
                    enoughMoneyIsInserted()
                        .assertThat(productWasReleased(shelf));
                }

                @Test public void no_money_is_returned()
                {
                    enoughMoneyIsInserted()
                        .assertThat(noCoinWasReturned());
                }

                @Test public void and_goodbye_message_is_displayed()
                {
                    enoughMoneyIsInserted()
                        .assertThat(messageWasDisplayed(Messages.AFTER_PURCHASE));
                }
            }

            public static class and_too_much_money_is_inserted_when_there_is_not_enough_change
            {
                private TestBuilder tooMuchMoneyIsInserted()
                {
                    return shelfWithProductsSelected().and(coinInserted(tooMuch));
                }

                @Test public void then_no_product_is_released()
                {
                    tooMuchMoneyIsInserted()
                        .assertThat(noProductWasReleased());
                }

                @Test public void and_money_is_returned()
                {
                    tooMuchMoneyIsInserted()
                        .assertThat(coinWasReturned(tooMuch));
                }

                @Test public void and_apologies_message_is_displayed()
                {
                    tooMuchMoneyIsInserted()
                        .assertThat(messageWasDisplayed(Messages.NO_CHANGE_MESSAGE));
                }
            }


            public static class and_too_little_money_is_inserted
            {
                private static TestBuilder tooLittleMoneyIsInserted()
                {
                    return shelfWithProductsSelected().and(coinInserted(tooLittle));
                }

                @Test public void then_no_product_is_released()
                {
                    tooLittleMoneyIsInserted()
                        .assertThat(noProductWasReleased());
                }

                @Test public void and_no_money_is_returned()
                {
                    tooLittleMoneyIsInserted()
                        .assertThat(noCoinWasReturned());
                }

                @Test public void and_remaining_amount_is_displayed()
                {
                    Money remaining = price.subtract(tooLittle.value());
                    tooLittleMoneyIsInserted()
                        .assertThat(messageContainingTextWasDisplayed(remaining.toString()));
                }

                public static class and_cancel_was_pressed
                {
                    private static TestBuilder userPressedCancel()
                    {
                        return tooLittleMoneyIsInserted().and(cancelPressed());
                    }

                    @Test public void then_no_product_is_released()
                    {
                        userPressedCancel()
                            .assertThat(noProductWasReleased());
                    }

                    @Test public void and_money_is_returned()
                    {
                        userPressedCancel()
                            .assertThat(coinWasReturned(tooLittle));
                    }
                }
            }
        }
    }


    private static OperationBuilder with(Consumer<MachineSetup> preparator)
    {
        return new OperationBuilder(preparator);
    }

    private static class OperationBuilder
    {
        private final Consumer<MachineSetup> preparator;

        private OperationBuilder(Consumer<MachineSetup> preparator)
        {
            this.preparator = preparator;
        }

        private TestBuilder after(Consumer<VendingMachine> operator)
        {
            return new TestBuilder(preparator, operator);
        }
    }

    private static class TestBuilder
    {
        private final Consumer<MachineSetup> preparator;
        private final Consumer<VendingMachine> operator;

        private TestBuilder(Consumer<MachineSetup> preparator, Consumer<VendingMachine> operator)
        {
            this.preparator = preparator;
            this.operator = operator;
        }

        private TestBuilder and(Consumer<VendingMachine> continuation)
        {
            return new TestBuilder(preparator, operator.andThen(continuation));
        }

        private void assertThat(Consumer<MachineSetup> verifier)
        {
            machineTest(preparator, operator, verifier);
        }
    }

    private static class MachineSetup
    {
        private Display display = mock(Display.class);
        private CashHolder bank = mock(CashHolder.class);
        private ProductHolder products = mock(ProductHolder.class);
        private SimpleShelfInformation productInfo = SimpleShelfInformation.create();
        private SimplePrices prices = SimplePrices.create();

        {
            when(products.numberOfShelves()).thenReturn(10);
            when(products.shelfCapacity()).thenReturn(10);
        }

        private VendingMachine buildMachine()
        {
            return VendingMachineBuilder.withDisplay(display)
                .withMoneyIn(bank)
                .withProducts(products, productInfo)
                .withPrices(prices);
        }
    }
}
