package tdd.vending.machine.state;

import org.junit.Test;
import tdd.vending.machine.AbstractVendingMachine;
import tdd.vending.money.*;

import org.assertj.core.api.Assertions;
import static org.mockito.Mockito.*;
import static tdd.vending.machine.state.StateTransitionVerifier.assertThat;

public class AbstractStateTest
{
    @Test public void userMessage_returns_message_specified_in_constructor()
    {
        Setup setup = new Setup();
        AbstractState state = setup.state;
        String message = setup.message;

        Assertions.assertThat(state.userMessage()).isEqualTo(message);
    }

    @Test public void cancelSelected_does_not_change_state()
    {
        Setup setup = new Setup();
        AbstractState state = setup.state;

        StateTransition transition = state.cancelSelected();

        assertThat(transition)
            .isNoTransition();
    }

    @Test public void cancelSelected_does_not_release_any_products()
    {
        Setup setup = new Setup();
        AbstractState state = setup.state;
        AbstractVendingMachine machine = setup.machine;

        state.cancelSelected();

        verify(machine, never()).dropProductFromShelf(anyInt());
    }

    @Test public void cancelSelected_does_not_release_any_money()
    {
        Setup setup = new Setup();
        AbstractState state = setup.state;
        AbstractVendingMachine machine = setup.machine;

        state.cancelSelected();

        verify(machine, never()).returnChange(any());
    }

    @Test public void returnChange_calls_machine_to_return_specified_amount()
    {
        Setup setup = new Setup();
        AbstractState state = setup.state;
        AbstractVendingMachine machine = setup.machine;
        Money amount = Money.money(2, 70);

        state.returnChange(amount);

        verify(machine).returnChange(amount);
    }

    @Test public void returnCoin_calls_machine_to_return_the_value_of_coin()
    {
        Setup setup = new Setup();
        AbstractState state = setup.state;
        AbstractVendingMachine machine = setup.machine;
        Coin coin = Coin.TEN_CENTS;

        state.returnCoin(coin);

        verify(machine).returnChange(coin.value());
    }

    @Test public void longDelayThenBackToThis()
    {
        Setup setup = new Setup();
        AbstractState state = setup.state;
        State temporaryState = State.dummy();

        StateTransition transition = state.longDelayThenBackToThis(temporaryState);

        assertThat(transition)
            .isTemporaryTransition()
            .toState(temporaryState)
            .andFinallyToState(state)
            .thatLasts(StateTransition.LONG_DELAY_SECONDS);
    }

    @Test public void defaultDelayThenBackToThis()
    {
        Setup setup = new Setup();
        AbstractState state = setup.state;
        State temporaryState = State.dummy();

        StateTransition transition = state.defaultDelayThenBackToThis(temporaryState);

        assertThat(transition)
            .isTemporaryTransition()
            .toState(temporaryState)
            .andFinallyToState(state)
            .thatLasts(StateTransition.DEFAULT_DELAY_SECONDS);
    }

    @Test public void displayMessageThenBackToThis()
    {
        Setup setup = new Setup();
        AbstractState state = setup.state;
        String message = "Some Fancy Message";

        StateTransition transition = state.displayMessageThenBackToThis(message);

        assertThat(transition)
            .isTemporaryTransition()
            .toStateDisplayingMessage(message)
            .andFinallyToState(state)
            .thatLasts(StateTransition.DEFAULT_DELAY_SECONDS);
    }

    @Test public void displayMessageThenSwitchTo()
    {
        Setup setup = new Setup();
        AbstractState state = setup.state;
        State finalState = State.dummy();
        String message = "Some Fancy Message";

        StateTransition transition = state.displayMessageThenSwitchTo(message, finalState);

        assertThat(transition)
            .isTemporaryTransition()
            .toStateDisplayingMessage(message)
            .andFinallyToState(finalState)
            .thatLasts(StateTransition.DEFAULT_DELAY_SECONDS);
    }

    private static class Setup
    {
        private final String message = "Some Message";
        private final AbstractVendingMachine machine = mock(AbstractVendingMachine.class);
        private final AbstractState state = new AbstractState(machine, message)
        {
            @Override public StateTransition coinInserted(Coin coin)
            {
                throw error();
            }

            @Override public StateTransition shelfSelected(int shelf)
            {
                throw error();
            }

            private RuntimeException error()
            {
                return new UnsupportedOperationException("This method should not be called during test.");
            }
        };
    }
}