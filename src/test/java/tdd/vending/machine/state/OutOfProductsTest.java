package tdd.vending.machine.state;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import tdd.vending.machine.*;
import tdd.vending.money.Coin;

import static org.mockito.Mockito.*;
import static tdd.vending.machine.state.StateTransitionVerifier.assertThat;

public class OutOfProductsTest
{
    @Test public void userMessage_returns_Messages$OUT_OF_PRODUCTS()
    {
        Setup setup = new Setup();

        OutOfProducts state = setup.state;

        Assertions.assertThat(state.userMessage()).isEqualTo(Messages.OUT_OF_PRODUCTS);
    }

    @Test public void coinInserted_returns_money_to_the_user()
    {
        Setup setup = new Setup();

        OutOfProducts state = setup.state;
        Coin coin = Coin.FIVE;

        state.coinInserted(coin);

        verify(setup.machine).returnChange(coin.value());
    }

    @Test public void coinInserted_temporarily_displays_warning()
    {
        Setup setup = new Setup();

        OutOfProducts state = setup.state;
        Coin coin = Coin.FIVE;

        StateTransition transition = state.coinInserted(coin);

        assertThat(transition)
            .isTemporaryTransition()
            .toStateDisplayingMessage(Messages.AFTER_COIN_INSERTED_WHEN_OUT_OF_PRODUCTS)
            .andFinallyToState(state)
            .thatLasts(StateTransition.DEFAULT_DELAY_SECONDS);
    }

    @Test public void cancelSelected_does_not_return_any_money()
    {
        Setup setup = new Setup();
        OutOfProducts state = setup.state;

        state.cancelSelected();

        verify(setup.machine, never()).returnChange(any());
    }

    @Test public void cancelSelected_does_not_change_state()
    {
        Setup setup = new Setup();
        OutOfProducts state = setup.state;

        StateTransition transition = state.cancelSelected();

        assertThat(transition)
            .isNoTransition();
    }

    private static class Setup
    {
        private final AbstractVendingMachine machine = mock(AbstractVendingMachine.class);
        private final OutOfProducts state = OutOfProducts.in(machine);
    }
}