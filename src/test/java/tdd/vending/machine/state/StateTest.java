package tdd.vending.machine.state;

import org.junit.Test;
import org.mockito.Mockito;
import tdd.vending.machine.*;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static tdd.vending.machine.state.State.*;
import static org.assertj.core.api.Assertions.*;

public class StateTest
{
    @Test
    public void initialStateFor_return_OutOfProducts_for_empty_machine()
    {
        Setup setup = new Setup();
        AbstractVendingMachine machine = setup.machine;

        when(machine.areShelvesEmpty()).thenReturn(true);

        assertThat(initialStateFor(machine)).isInstanceOf(OutOfProducts.class);
    }

    @Test
    public void initialStateFor_return_WaitingForUser_for_non_empty_machine()
    {
        Setup setup = new Setup();
        AbstractVendingMachine machine = setup.machine;

        assertThat(initialStateFor(machine)).isInstanceOf(WaitingForUserAction.class);
    }

    @Test
    public void dummy_returns_instanceof_DummyState()
    {
        assertThat(dummy()).isInstanceOf(DummyState.class);
    }

    @Test
    public void isDummy_returns_true_for_instance_of_DummyState()
    {
        DummyState dummy = new DummyState();

        assertThat(isDummy(dummy)).isTrue();
    }

    @Test
    public void isDummy_returns_false_for_instances_other_than_DummyState()
    {
        AbstractVendingMachine machine = Mockito.mock(AbstractVendingMachine.class);
        OutOfProducts state = OutOfProducts.in(machine);

        assertThat(isDummy(state)).isFalse();
    }

    private static class Setup
    {
        AbstractVendingMachine machine = mock(AbstractVendingMachine.class);
    }
}