package tdd.vending.machine.state;

import org.junit.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static tdd.vending.machine.state.StateTransition.*;
import static org.assertj.core.api.Assertions.*;

public class StateTransitionTest
{
    @Test
    public void none_returns_transition_that_does_not_call_any_handlers() throws Exception
    {
        StateTransition trans = none();

        NormalTransitionHandler normalHandler = mock(NormalTransitionHandler.class);
        TemporaryTransitionHandler transientHandler = mock(TemporaryTransitionHandler.class);

        trans.whenNormal(normalHandler);
        trans.whenTemporary(transientHandler);

        verify(normalHandler, never()).accept(any());
        verify(transientHandler, never()).accept(any(), any(), anyInt());
    }

    @Test
    public void normal_return_transition_that_calls_normal_handler_with_specified_state() throws Exception
    {
        State state = mock(State.class);
        StateTransition trans = normal(state);

        NormalTransitionHandler normalHandler = mock(NormalTransitionHandler.class);
        TemporaryTransitionHandler transientHandler = mock(TemporaryTransitionHandler.class);

        trans.whenNormal(normalHandler);
        trans.whenTemporary(transientHandler);

        verify(normalHandler).accept(state);
        verify(transientHandler, never()).accept(any(), any(), anyInt());
    }

    @Test
    public void temporary_returns_transition_that_calls_transient_handler_with_specified_states_and_delays() throws Exception
    {
        State state1 = mock(State.class);
        State state2 = mock(State.class);
        int delay = 73;
        StateTransition trans = temporary(state1, state2, delay);

        NormalTransitionHandler normalHandler = mock(NormalTransitionHandler.class);
        TemporaryTransitionHandler transientHandler = mock(TemporaryTransitionHandler.class);

        trans.whenNormal(normalHandler);
        trans.whenTemporary(transientHandler);

        verify(normalHandler, never()).accept(any());
        verify(transientHandler).accept(state1, state2, delay);
    }

    @Test
    public void temporary_throws_IllegalArgumentException_for_non_positive_delay()
    {
        State state1 = mock(State.class);
        State state2 = mock(State.class);

        assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> temporary(state1, state2, 0));
        assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> temporary(state1, state2, -1));
    }
}