package tdd.vending.machine.state;

import tdd.vending.machine.state.StateTransition.*;

import org.assertj.core.api.Assertions;

/**
 * Test utility class for creating assertions about state transition.
 *
 * Usage example:
 * <code>
 *     StateTransition transition = state.doSomething();
 *     assertThat(transition)
 *         .isTemporaryTransition()
 *         .toState(someState)
 *         .andFinallyToStateOfType(SomeOtherState.class)
 *         .thatLasts(10);
 * </code>
 */
public class StateTransitionVerifier
{
    public static TransitionVerifier assertThat(StateTransition transition)
    {
        return new TransitionVerifier(transition);
    }

    public static class TransitionVerifier
    {
        private final StateTransition transition;

        private TransitionVerifier(StateTransition transition)
        {
            this.transition = transition;
        }

        public void isNoTransition()
        {
            Assertions.assertThat(transition).isNotInstanceOf(Normal.class);
            Assertions.assertThat(transition).isNotInstanceOf(Temporary.class);
        }

        public NormalTransitionVerifier isNormalTransition()
        {
            Assertions.assertThat(transition).isInstanceOf(Normal.class);
            return new NormalTransitionVerifier((Normal) transition);
        }

        public TemporaryStateVerifier isTemporaryTransition()
        {
            Assertions.assertThat(transition).isInstanceOf(Temporary.class);
            return new TemporaryStateVerifier((Temporary) transition);
        }
    }

    public static class NormalTransitionVerifier
    {
        private final Normal normal;

        private NormalTransitionVerifier(Normal normal)
        {
            this.normal = normal;
        }

        public void toStateOfType(Class<? extends State> clazz)
        {
            Assertions.assertThat(normal.nextState).isInstanceOf(clazz);
        }

        public void toState(State state)
        {
            Assertions.assertThat(normal.nextState).isEqualTo(state);
        }

        public void toStateOfTheSameType(State state)
        {
            toStateOfType(state.getClass());
        }
    }

    public static class TemporaryStateVerifier extends TemporaryTransitionTimeVerifier
    {
        private final Temporary temporary;

        private TemporaryStateVerifier(Temporary temporary)
        {
            super(temporary.transitionSeconds);
            this.temporary = temporary;
        }

        public FinalStateVerifier toStateOfType(Class<? extends State> clazz)
        {
            Assertions.assertThat(temporary.temporaryState).isInstanceOf(clazz);
            return new FinalStateVerifier(temporary);
        }

        public FinalStateVerifier toState(State state)
        {
            Assertions.assertThat(temporary.temporaryState).isEqualTo(state);
            return new FinalStateVerifier(temporary);
        }

        public FinalStateVerifier toStateDisplayingMessage(String message)
        {
            Assertions.assertThat(temporary.temporaryState.userMessage())
                .isEqualTo(message);
            return new FinalStateVerifier(temporary);
        }
    }

    public static class FinalStateVerifier
    {
        private final Temporary temporary;

        private FinalStateVerifier(Temporary temporary)
        {
            this.temporary = temporary;
        }

        public TemporaryTransitionTimeVerifier andFinallyToStateOfType(Class<? extends State> clazz)
        {
            Assertions.assertThat(temporary.finalState).isInstanceOf(clazz);
            return new TemporaryTransitionTimeVerifier(temporary.transitionSeconds);
        }

        public TemporaryTransitionTimeVerifier andFinallyToState(State state)
        {
            Assertions.assertThat(temporary.finalState).isEqualTo(state);
            return new TemporaryTransitionTimeVerifier(temporary.transitionSeconds);
        }

        public TemporaryTransitionTimeVerifier andFinallyToStateOfTheSameType(State state)
        {
            return andFinallyToStateOfType(state.getClass());
        }
    }

    public static class TemporaryTransitionTimeVerifier
    {
        private final int duration;

        private TemporaryTransitionTimeVerifier(int duration)
        {
            this.duration = duration;
        }

        public void thatLasts(int time)
        {
            Assertions.assertThat(duration).isEqualTo(time);
        }
    }
}
