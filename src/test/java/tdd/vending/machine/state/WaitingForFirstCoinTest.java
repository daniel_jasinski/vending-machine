package tdd.vending.machine.state;

import org.junit.Test;
import org.assertj.core.api.Assertions;
import org.mockito.Mockito;
import tdd.vending.machine.AbstractVendingMachine;
import tdd.vending.money.*;
import tdd.vending.products.ProductType;
import java.util.Optional;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;
import static tdd.vending.machine.state.StateTransitionVerifier.assertThat;
import static tdd.vending.money.Money.*;

public class WaitingForFirstCoinTest
{
    private final static int DEFAULT_SHELF = 0;
    private final static ProductType DEFAULT_PRODUCT = ProductType.withName("Cola");
    private final static Money DEFAULT_PRICE = cents(50);

    private WaitingForFirstCoin defaultInstance()
    {
        return defaultInstance(DEFAULT_PRICE);
    }

    private WaitingForFirstCoin defaultInstance(Money price)
    {
        return defaultInstance(mock(AbstractVendingMachine.class), price);
    }

    private WaitingForFirstCoin defaultInstance(AbstractVendingMachine machine, Money price)
    {
        when(machine.productOnShelf(anyInt())).thenReturn(Optional.of(DEFAULT_PRODUCT));
        when(machine.priceOf(Mockito.any())).thenReturn(Optional.of(price));

        return WaitingForFirstCoin.in(machine, DEFAULT_SHELF, DEFAULT_PRODUCT, price);
    }

    @Test
    public void userMessage_contains_price() throws Exception
    {
        WaitingForFirstCoin state = defaultInstance();
        Assertions.assertThat(state.userMessage()).contains(DEFAULT_PRICE.toString());
    }

    @Test
    public void coinInserted_switches_to_InsertingCoins_when_single_coin_is_not_enough() throws Exception
    {
        Coin coin = Coin.TEN_CENTS;
        Money price = cents(20);
        WaitingForFirstCoin state = defaultInstance(price);

        StateTransition trans = state.coinInserted(coin);

        assertThat(trans)
            .isNormalTransition()
            .toStateOfType(InsertingCoins.class);
    }

    @Test
    public void coinInserted_switches_to_WaitingForUserAction_when_single_coin_is_enough() throws Exception
    {
        Coin coin = Coin.TEN_CENTS;
        Money price = coin.value();
        WaitingForFirstCoin state = defaultInstance(price);

        StateTransition transition = state.coinInserted(coin);

        assertThat(transition)
            .isTemporaryTransition()
            .toStateOfType(TransientState.class)
            .andFinallyToStateOfType(WaitingForUserAction.class);
    }

    @Test
    public void coinInserted_drops_product_when_single_coin_is_enough() throws Exception
    {
        Coin coin = Coin.TEN_CENTS;
        Money price = coin.value();

        AbstractVendingMachine machine = mock(AbstractVendingMachine.class);
        WaitingForFirstCoin state = defaultInstance(machine, price);

        state.coinInserted(coin);

        verify(machine).dropProductFromShelf(DEFAULT_SHELF);
    }


    @Test
    public void coinInserted_does_not_return_change_when_single_coin_is_enough() throws Exception
    {
        Coin coin = Coin.TEN_CENTS;
        Money price = coin.value();

        AbstractVendingMachine machine = mock(AbstractVendingMachine.class);
        WaitingForFirstCoin state = defaultInstance(machine, price);

        state.coinInserted(coin);

        verify(machine, never()).returnChange(any());
    }

    @Test
    public void coinInserted_drops_product_and_returns_proper_change_when_single_coin_is_more_than_enough() throws Exception
    {
        Coin coin = Coin.TWENTY_CENTS;
        Money price = cents(10);
        Money change = cents(10);

        AbstractVendingMachine machine = mock(AbstractVendingMachine.class);
        when(machine.canReturnChange(any())).thenReturn(true);

        WaitingForFirstCoin state = defaultInstance(machine, price);

        state.coinInserted(coin);

        verify(machine).returnChange(change);
        verify(machine).dropProductFromShelf(DEFAULT_SHELF);
    }

    @Test
    public void coinInserted_returns_inserted_money_when_cannot_return_change() throws Exception
    {
        Coin coin = Coin.TWENTY_CENTS;
        Money price = cents(10);

        AbstractVendingMachine machine = mock(AbstractVendingMachine.class);

        WaitingForFirstCoin state = defaultInstance(machine, price);

        state.coinInserted(coin);

        verify(machine).returnChange(coin.value());
        verify(machine, never()).dropProductFromShelf(anyInt());
    }

    @Test
    public void cancelSelected_switches_to_WaitingForUserAction() throws Exception
    {
        WaitingForFirstCoin state = defaultInstance();

        StateTransition transition = state.cancelSelected();

        assertThat(transition)
            .isNormalTransition()
            .toStateOfType(WaitingForUserAction.class);
    }

    @Test
    public void shelfSelected_switches_to_the_same_state_with_different_shelf() throws Exception
    {
        WaitingForFirstCoin state = defaultInstance();

        StateTransition transition = state.shelfSelected(0);

        assertThat(transition)
            .isTemporaryTransition()
            .toStateOfType(WaitingForFirstCoin.class)
            .andFinallyToStateOfType(WaitingForUserAction.class)
            .thatLasts(StateTransition.LONG_DELAY_SECONDS);
    }
}