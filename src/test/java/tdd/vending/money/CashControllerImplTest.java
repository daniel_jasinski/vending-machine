package tdd.vending.money;

import org.junit.Test;
import tdd.vending.hardware.CashHolder;
import java.util.*;

import static org.mockito.Mockito.*;
import static tdd.vending.money.Coin.*;
import static tdd.vending.money.Money.*;
import static org.assertj.core.api.Assertions.*;

public class CashControllerImplTest
{
    private CashControllerImpl buildInstance()
    {
        CashHolder holder = mock(CashHolder.class);
        return CashControllerImpl.using(holder);
    }

    @Test
    public void after_creation_is_empty()
    {
        CashControllerImpl box = buildInstance();
        assertThat(box.totalAmount()).isEqualTo(ZERO);
        for(Coin coin: values())
            assertThat(box.numberOf(coin)).isEqualTo(0);
    }

    @Test
    public void after_inserting_ONE_and_TWO_totalAmount_is_3()
    {
        CashControllerImpl box = buildInstance();
        box.coinInserted(ONE);
        box.coinInserted(TWO);
        assertThat(box.totalAmount()).isEqualTo(whole(3));
    }


    @Test
    public void reset_zeros_the_total_amount()
    {
        CashControllerImpl box = buildInstance();
        box.coinInserted(ONE);
        box.coinInserted(TWO);
        box.reset();
        assertThat(box.totalAmount()).isEqualTo(ZERO);
    }

    @Test
    public void empty_cannot_retrieve_any_amount()
    {
        CashControllerImpl box = buildInstance();
        assertThat(box.canReturnChange(cents(10))).isFalse();
        assertThat(box.canReturnChange(cents(20))).isFalse();
        assertThat(box.canReturnChange(cents(50))).isFalse();
        assertThat(box.canReturnChange(whole(1))).isFalse();
        assertThat(box.canReturnChange(money(3, 50))).isFalse();
    }

    @Test
    public void canRetrieve_throws_IllegalArgumentException_for_negative_amount()
    {
        CashControllerImpl box = buildInstance();
        assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> box.canReturnChange(whole(-1)));
    }

    @Test
    public void retrieve_throws_IllegalArgumentException_for_negative_amount()
    {
        CashControllerImpl box = buildInstance();
        assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> box.returnChange(whole(-1)));
    }

    @Test
    public void retrieve_throws_IllegalStateException_when_given_amount_cannot_be_retrieved()
    {
        CashControllerImpl box = buildInstance();
        assertThatExceptionOfType(IllegalStateException.class)
            .isThrownBy(() -> box.returnChange(whole(1)));
    }

    @Test
    public void retrieve_returns_correct_coins() throws Exception
    {
        CashHolder mock = mock(CashHolder.class);
        CashControllerImpl box = CashControllerImpl.using(mock);
        box.coinInserted(TWENTY_CENTS);
        box.coinInserted(TWENTY_CENTS);
        box.coinInserted(TWENTY_CENTS);
        box.coinInserted(TWENTY_CENTS);
        box.coinInserted(TWENTY_CENTS);
        box.coinInserted(TEN_CENTS);
        box.coinInserted(TEN_CENTS);
        box.coinInserted(TEN_CENTS);

        List<Coin> coins = new ArrayList<>();
        doAnswer(invocation -> {
            coins.add((Coin) invocation.getArguments()[0]);
            return null;
        }).when(mock).returnChange(any());

        box.returnChange(cents(70));

        assertThat(coins.size()).isEqualTo(4);
        assertThat(coins.get(0)).isEqualTo(TWENTY_CENTS);
        assertThat(coins.get(1)).isEqualTo(TWENTY_CENTS);
        assertThat(coins.get(2)).isEqualTo(TWENTY_CENTS);
        assertThat(coins.get(3)).isEqualTo(TEN_CENTS);
    }
}