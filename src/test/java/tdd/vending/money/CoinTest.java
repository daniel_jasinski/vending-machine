package tdd.vending.money;

import org.junit.Test;

import static tdd.vending.money.Coin.*;
import static tdd.vending.money.Money.*;
import static org.assertj.core.api.Assertions.*;

public class CoinTest
{
    @Test
    public void toString_is_delegated_to_the_value()
    {
        for(Coin c: values())
            assertThat(c.toString().equals(c.value().toString())).isTrue();
    }

    @Test
    public void FIVE_is_really_worth_5() throws Exception
    {
        assertThat(FIVE.value().isEqual(money(5,0))).isTrue();
    }

    @Test
    public void TWO_is_really_worth_2() throws Exception
    {
        assertThat(TWO.value().isEqual(money(2,0))).isTrue();
    }

    @Test
    public void ONE_is_really_worth_1() throws Exception
    {
        assertThat(ONE.value().isEqual(money(1,0))).isTrue();
    }

    @Test
    public void FIFTY_CENTS_is_really_worth_50_cents() throws Exception
    {
        assertThat(FIFTY_CENTS.value().isEqual(cents(50))).isTrue();
    }
    @Test
    public void TWENTY_CENTS_is_really_worth_20_cents() throws Exception
    {
        assertThat(TWENTY_CENTS.value().isEqual(cents(20))).isTrue();
    }

    @Test
    public void TEN_CENTS_is_really_worth_10_cents() throws Exception
    {
        assertThat(TEN_CENTS.value().isEqual(cents(10))).isTrue();
    }
}