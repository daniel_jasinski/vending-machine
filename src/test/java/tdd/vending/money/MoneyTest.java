package tdd.vending.money;

import org.junit.Test;

import static tdd.vending.money.Money.*;
import static org.assertj.core.api.Assertions.*;

public class MoneyTest
{
    @Test
    public void money_do_not_allow_negative_amounts()
    {
        assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> money(1,-10));
        assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> money(-1,10));
        assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> money(-1,-10));
    }

    @Test
    public void money_do_not_allow_amount_greater_or_equal_100()
    {
        assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> money(0,100));
    }

    @Test
    public void cents_do_not_allow_amount_greater_or_equal_100()
    {
        assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> cents(100));
    }

    @Test
    public void cents_and_money_return_equivalent_amounts()
    {
        int amount = 70;
        assertThat(cents(amount)).isEqualTo(money(0, amount));
    }

    @Test
    public void whole_and_money_return_equivalent_amounts()
    {
        int amount = 3;
        assertThat(whole(amount)).isEqualTo(money(amount, 0));
    }

    @Test
    public void add_returns_correct_amount()
    {
        assertThat(money(2, 30).add(cents(50))).isEqualTo(money(2,80));
    }

    @Test
    public void subtract_returns_correct_amount()
    {
        assertThat(money(2, 30).subtract(cents(50))).isEqualTo(money(1,80));
    }

    @Test
    public void times_returns_multiplied_amount()
    {
        assertThat(money(1,10).times(7)).isEqualTo(money(7, 70));
    }

    @Test
    public void negate_returns_amount_with_the_opposite_sign()
    {
        int amount = 3;
        Money positive = Money.whole(amount);
        Money negative = Money.whole(-amount);

        assertThat(positive.negate()).isEqualTo(negative);
    }

    @Test
    public void twenty_cents_is_five_times_inside_one()
    {
        Money one = whole(1);
        Money twentyCents = cents(20);
        assertThat(twentyCents.numberOfTimesContainedIn(one)).isEqualTo(5);
    }

    @Test
    public void one_is_zero_times_inside_fifty_cents()
    {
        Money one = whole(1);
        Money fiftyCents = cents(50);
        assertThat(one.numberOfTimesContainedIn(fiftyCents)).isEqualTo(0);
    }

    @Test
    public void isEqual_returns_true_for_the_same_amount()
    {
        int amount = 7;
        assertThat(whole(amount).isEqual(whole(amount))).isTrue();
    }

    @Test
    public void isEqual_returns_false_for_different_amounts()
    {
        Money m1 = whole(3);
        Money m2 = whole(4);
        assertThat(m1.isEqual(m2)).isFalse();
    }

    @Test
    public void ZERO_is_really_zero()
    {
        assertThat(ZERO.equals(money(0, 0))).isTrue();
    }

    @Test
    public void ZERO_is_less_than_positive_amount()
    {
        assertThat(ZERO.isLessThan(whole(1))).isTrue();
    }

    @Test
    public void ZERO_is_greater_than_negative_amount()
    {
        assertThat(ZERO.isGreaterThan(whole(-1))).isTrue();
    }

    @Test
    public void ZERO_is_less_or_equal_zero()
    {
        assertThat(ZERO.isLessOrEqual(whole(0))).isTrue();
    }

    @Test
    public void ZERO_is_greater_or_equal_zero()
    {
        assertThat(ZERO.isGreaterOrEqual(whole(0))).isTrue();
    }

    @Test
    public void fractional_amount_is_formatted_with_leading_and_trailing_zeros()
    {
        assertThat(cents(30).toString()).isEqualTo("0.30");
    }
}