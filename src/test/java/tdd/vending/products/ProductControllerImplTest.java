package tdd.vending.products;

import org.junit.Test;
import tdd.vending.hardware.ProductHolder;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static tdd.vending.products.ProductControllerImpl.*;
import static org.assertj.core.api.Assertions.*;
import static tdd.vending.util.Verifiers.assertThatOptional;

public class ProductControllerImplTest
{
    @Test
    public void after_creation_all_shelves_are_empty_and_product_types_are_null()
    {
        ProductControllerImpl ctrl = fromHolder(defaultHolder());

        for(int i = 0; i<ctrl.numberOfShelves(); i++)
        {
            assertThat(ctrl.isShelfEmpty(i)).isTrue();
            assertThatOptional(ctrl.productOnShelf(i)).isEmpty();
        }
    }

    @Test
    public void areAllShelvesEmpty_returns_true_after_creation()
    {
        ProductControllerImpl ctrl = defaultInstance();

        assertThat(ctrl.areAllShelvesEmpty()).isTrue();
    }

    @Test
    public void areAllShelvesEmpty_returns_true_after_initialization()
    {
        ProductControllerImpl ctrl = defaultInstance();

        ctrl.resetTo(defaultProvider());

        assertThat(ctrl.areAllShelvesEmpty()).isFalse();
    }

    @Test
    public void after_resetTo_shelves_contain_only_what_was_specified_in_the_provider()
    {
        ProductControllerImpl ctrl = fromHolder(defaultHolder());
        int shelf = 3;
        int numberOfItems = 4;
        ShelfContent info = ShelfContent.of(ProductType.withName("Cola"), numberOfItems);
        ShelfInformation provider = providerOf(info, shelf);

        ctrl.resetTo(provider);

        for(int i = 0; i<ctrl.numberOfShelves(); i++)
        {
            if(i == shelf)
            {
                assertThat(ctrl.numberOfProductsOnShelf(shelf)).isEqualTo(info.numberOfItems);
                assertThatOptional(ctrl.productOnShelf(shelf)).contains(info.productType);
            }
            else
            {
                assertThat(ctrl.isShelfEmpty(i)).isTrue();
                assertThatOptional(ctrl.productOnShelf(i)).isEmpty();
            }
        }
    }

    @Test public void numberOfShelves_throws_InvalidArgumentException_for_negative_indexes() throws Exception
    {
        ProductControllerImpl ctx = defaultInstance();

        assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> ctx.numberOfProductsOnShelf(-1));
    }

    @Test
    public void numberOfProductOnShelf_number_provider_from_holder() throws Exception
    {
        ProductHolder holder = defaultHolder();
        ProductControllerImpl ctx = fromHolder(holder);

        assertThat(ctx.numberOfShelves()).isEqualTo(holder.numberOfShelves());
    }

    @Test
    public void productOnShelf_throws_InvalidArgumentException_for_negative_indexes() throws Exception
    {
        ProductControllerImpl ctx = defaultInstance();

        assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> ctx.productOnShelf(-1));
    }

    @Test
    public void productOnShelf_throws_InvalidArgumentException_greater_or_equal_shelf_number() throws Exception
    {
        ProductControllerImpl ctx = defaultInstance();
        int size = ctx.numberOfShelves();

        assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> ctx.productOnShelf(size));
        assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> ctx.productOnShelf(size+1));
    }

    @Test
    public void dropProductFromShelf_throws_InvalidArgumentException_for_negative_indexes() throws Exception
    {
        ProductControllerImpl ctx = defaultInstance();

        assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> ctx.dropProductFromShelf(-1));
    }

    @Test
    public void dropProductFromShelf_throws_InvalidArgumentException_greater_or_equal_shelf_number() throws Exception
    {
        ProductControllerImpl ctx = defaultInstance();
        int size = ctx.numberOfShelves();

        assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> ctx.dropProductFromShelf(size));
        assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> ctx.dropProductFromShelf(size+1));
    }

    @Test
    public void dropProductFromShelf_throws_IllegalStateException_when_called_on_empty_shelf() throws Exception
    {
        ProductControllerImpl ctx = defaultInstance();

        assertThatExceptionOfType(IllegalStateException.class)
            .isThrownBy(() -> ctx.dropProductFromShelf(0));
    }

    @Test
    public void dropProductFromShelf_reduces_number_of_products_on_the_shelf_by_one() throws Exception
    {
        int initialNumber = 5;
        int shelf = 0;
        ProductType type = ProductType.withName("Cola");
        ShelfContent info = ShelfContent.of(type, initialNumber);
        ProductControllerImpl ctx = defaultInstance();
        ShelfInformation provider = providerOf(info, shelf);
        ctx.resetTo(provider);

        ctx.dropProductFromShelf(shelf);

        assertThat(ctx.numberOfProductsOnShelf(shelf)).isEqualTo(initialNumber - 1);
    }

    private ProductHolder holderOfSize(int numberOfShelves, int shelfCapacity)
    {
        ProductHolder holder = mock(ProductHolder.class);
        when(holder.numberOfShelves()).thenReturn(numberOfShelves);
        when(holder.shelfCapacity()).thenReturn(shelfCapacity);
        return holder;
    }

    private ProductHolder defaultHolder()
    {
        return holderOfSize(10, 10);
    }

    private ProductControllerImpl defaultInstance()
    {
        return fromHolder(defaultHolder());
    }

    private ShelfInformation defaultProvider()
    {
        ShelfContent info = ShelfContent.of(ProductType.withName("Cola"), 1);
        return providerOf(info, 0);
    }

    private ShelfInformation providerOf(ShelfContent info, int shelf)
    {
        return s -> s == shelf ? Optional.of(info) : Optional.empty();
    }
}