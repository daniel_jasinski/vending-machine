package tdd.vending.products;

import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class ProductTypeTest
{
    @Test
    public void getName_returns_name_passed_to_the_factory_method() throws Exception
    {
        String name = "Some Fancy Product";
        ProductType type = ProductType.withName(name);
        assertThat(type.getName()).isEqualTo(name);
    }

    @Test
    public void toString_and_getName_return_the_same_value() throws Exception
    {
        ProductType type = ProductType.withName("Some Product");
        assertThat(type.getName()).isEqualTo(type.toString());
    }

    @Test
    public void types_with_the_same_name_are_equal() throws Exception
    {
        ProductType type1 = ProductType.withName("Product");
        ProductType type2 = ProductType.withName("Product");
        assertThat(type1).isEqualTo(type2);
    }
}