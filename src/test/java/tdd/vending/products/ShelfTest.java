package tdd.vending.products;

import org.junit.Test;

import static org.assertj.core.api.Assertions.*;
import static tdd.vending.util.Verifiers.*;

public class ShelfTest
{
    @Test
    public void withSize_throws_IllegalArgumentException_on_non_positive_size() throws Exception
    {
        assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> Shelf.ofSize(0));
        assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> Shelf.ofSize(-1));
    }

    @Test
    public void initialized_shelf_is_empty() throws Exception
    {
        Shelf shelf = Shelf.ofSize(10);
        assertThat(shelf.isEmpty()).isTrue();
    }

    @Test
    public void addItems_makes_shelf_not_empty() throws Exception
    {
        Shelf shelf = Shelf.ofSize(10);
        shelf.addItems(3, ProductType.withName("Cola"));
        assertThat(shelf.isEmpty()).isFalse();
    }

    @Test
    public void addItems_are_on_the_shelf() throws Exception
    {
        Shelf shelf = Shelf.ofSize(10);
        int N = 3;
        shelf.addItems(N, ProductType.withName("Cola"));
        assertThat(shelf.numberOfItems()).isEqualTo(N);
    }

    @Test
    public void addItems_adds_a_single_item() throws Exception
    {
        Shelf shelf = Shelf.ofSize(10);
        shelf.addItem(ProductType.withName("Cola"));
        assertThat(shelf.numberOfItems()).isEqualTo(1);
    }

    @Test
    public void addItems_with_different_type_throws_exception()
    {
        Shelf shelf = Shelf.ofSize(10);
        shelf.addItems(2, ProductType.withName("Cola"));
        assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> shelf.addItems(2, ProductType.withName("Pepsi")));
    }

    @Test
    public void clean_makes_shelf_empty() throws Exception
    {
        Shelf shelf = Shelf.ofSize(10);
        shelf.addItems(3, ProductType.withName("Cola"));
        shelf.clean();
        assertThat(shelf.isEmpty()).isTrue();
    }

    @Test
    public void empty_shelf_returns_null_product_type() throws Exception
    {
        Shelf shelf = Shelf.ofSize(10);
        assertThatOptional(shelf.productType()).isEmpty();
    }

    @Test
    public void productType_return_inserted_product() throws Exception
    {
        ProductType product = ProductType.withName("Cola");
        Shelf shelf = Shelf.ofSize(10);
        shelf.addItem(product);
        assertThatOptional(shelf.productType()).contains(product);
    }

    @Test
    public void getItem_decreases_number_of_items_by_one() throws Exception
    {
        ProductType product = ProductType.withName("Cola");
        Shelf shelf = Shelf.ofSize(10);
        int N = 3;
        shelf.addItems(N, product);
        shelf.removedItem();
        assertThat(shelf.numberOfItems()).isEqualTo(N - 1);
    }
}