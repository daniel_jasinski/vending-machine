package tdd.vending.products;

import org.junit.Test;
import tdd.vending.money.Money;
import java.util.Optional;

import static org.assertj.core.api.Assertions.*;
import static tdd.vending.util.Verifiers.assertThatOptional;

public class SimplePricesTest
{
    @Test
    public void priceOf_returns_Optional$empty_for_an_unknown_product()
    {
        SimplePrices prices = SimplePrices.create();
        ProductType unknown = ProductType.withName("Cola");

        Optional<Money> maybePrice = prices.priceOf(unknown);

        assertThat(maybePrice.isPresent()).isFalse();
    }

    @Test
    public void setPrice_throws_IllegalArgumentException_for_non_positive_price()
    {
        SimplePrices prices = SimplePrices.create();
        ProductType product = ProductType.withName("Cola");

        assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> prices.setPrice(product, Money.ZERO));
        assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> prices.setPrice(product, Money.whole(-1)));
    }

    @Test
    public void priceOf_returns_price_previously_set_by_setPrice()
    {
        SimplePrices prices = SimplePrices.create();
        Money price = Money.money(2, 50);
        ProductType product = ProductType.withName("Cola");

        prices.setPrice(product, price);

        Optional<Money> maybePrice = prices.priceOf(product);

        assertThatOptional(maybePrice).contains(price);
    }
}