package tdd.vending.util;

import org.junit.Test;
import org.junit.runners.Suite;
import org.junit.runners.model.RunnerBuilder;
import java.lang.reflect.*;
import java.util.*;

public class NestedRunner extends Suite
{
    public NestedRunner(Class<?> klass, RunnerBuilder builder) throws Throwable
    {
        super(builder, klass, extract(klass));
    }

    private static boolean isTestMethod(Method method)
    {
        if( ! Modifier.isPublic(method.getModifiers()))
            return false;
        if(Modifier.isStatic(method.getModifiers()))
            return false;
        if(method.getParameterCount() != 0)
            return false;
        if(method.getReturnType() != void.class)
            return false;
        return method.isAnnotationPresent(Test.class);
    }

    private static boolean containsTests(Class<?> clazz)
    {
        if(Modifier.isAbstract(clazz.getModifiers()))
            return false;
        for(Method m: clazz.getMethods())
            if(isTestMethod(m))
                return true;
        return false;
    }

    private static Class<?>[] extract(final Class<?> klass)
    {
        final Class<?>[] classes = klass.getClasses();
        final List<Class<?>> filtered = new ArrayList<Class<?>>(classes.length+1);

        extract(klass, filtered);

        return filtered.toArray(new Class<?>[filtered.size()]);
    }

    private static void extract(Class<?> klass, List<Class<?>> filtered)
    {
        if(containsTests(klass))
            filtered.add(klass);

        for (final Class<?> clazz : klass.getClasses())
            extract(clazz, filtered);
    }
}
