package tdd.vending.util;

import java.util.Optional;
import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

public class Verifiers
{
    /**
     * Method used for assertions about state of an <code>{@link Optional}</code>.
     *
     * Usage example:
     * <code>
     *     Optional<String> subject = Optional.empty();
     *
     *     assertThatOptional(subject).isEmpty();        //passes
     *     assertThatOptional(subject).contains("test"); //fails
     * </code>
     *
     * @param optional Optional instance that should be tested for content.
     */
    public static <E> OptionalVerifier<E> assertThatOptional(Optional<E> optional)
    {
        return new OptionalVerifier<>(optional);
    }

    /**
     * Class only used for convenient syntax of <code>{@link Optional}</code> assertions.
     */
    public static class OptionalVerifier<T>
    {
        private final Optional<T> optional;

        private OptionalVerifier(Optional<T> optional)
        {
            this.optional = optional;
        }

        public void isEmpty()
        {
            assertThat(optional.isPresent()).isFalse();
        }

        @SuppressWarnings("unchecked")
        public void contains(T element)
        {
            // Testing for returned value will be performed using
            // this delegate because Optional.get() will be deprecated
            // in the future and should be avoided.
            Consumer<T> spy = mock(Consumer.class);
            optional.ifPresent(spy);

            verify(spy).accept(element);
        }
    }
}
